/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.listas.ListaEnlazada;
import modelo.Persona;

/**
 *
 * @author cobos
 */
public class ControladorCliente {
    public static final transient String NOMBRE_ARCHIVO = "clientes";
    private ListaEnlazada<Persona> clienteLista = new ListaEnlazada<>();

    public ControladorCliente() {

    }

    public void insertarNuevoCliente(String nombre, String apellido, String cedula) {
        Persona aux = new Persona(nombre, apellido, cedula);
        System.out.println(aux);
        getClienteLista().insertar(aux);
    }

    public void buscar(Integer tipoBusqueda, String elemento, String atributo) {
        try {
            String nombre = null;
            String apellido = null;
            String cedula = null;
            Integer id = null;
            
            
            if (atributo.equalsIgnoreCase("id")) {
                id = Integer.valueOf(elemento);
            }else if(atributo.equalsIgnoreCase("nombres")){
                nombre = elemento;
            }else if(atributo.equalsIgnoreCase("apellidos")){
                apellido = elemento;
            }else if(atributo.equalsIgnoreCase("cedula")){
                cedula = elemento;
            }
            ListaEnlazada<Persona> aux = new ListaEnlazada<>();
            if (tipoBusqueda == 1) {
                aux.insertar((Persona) this.getClienteLista().busquedaBinaria(new Persona( id, nombre, apellido, cedula), false, atributo));
            } else {
                ListaEnlazada<Object> auxObject = getClienteLista().busquedaBinariaLineal(new Persona( id, nombre, apellido, cedula), false, atributo);
                for (int i = 0; i < auxObject.getSize(); i++) {
                    aux.insertar((Persona) auxObject.obtener(i));
                }
            }
            setClienteLista(aux);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public ListaEnlazada<Persona> getClienteLista() {
        return clienteLista;
    }

    public void setClienteLista(ListaEnlazada<Persona> clienteLista) {
        this.clienteLista = clienteLista;
    }

  
   
}
