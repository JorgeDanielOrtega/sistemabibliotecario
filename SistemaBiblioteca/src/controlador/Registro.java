/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.itextpdf.text.DocumentException;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Clase registro, sirve para crear los registros en pdf
 *
 * @author daniel
 */
public abstract class Registro {

    private static String DIRFOLDER = "pdf";
    protected String fullpath;
    protected String nombreArchivo;

    /**
     * Constructor con un parametro
     *
     * @param nombreArchivo Nombre del archivo como quiere que se guarde
     */
    public Registro(String nombreArchivo) {
        fullpath = DIRFOLDER + File.separatorChar + nombreArchivo + " - " + getFecha() + ".pdf";
    }

    /**
     * Crea en registro en pdf
     *
     * @param titulo Titulo que contendra el pdf
     * @return Retorna true si todo a saliido bien
     * @throws FileNotFoundException Excepcion que se lanza cuando el archivo no
     * ha sido encontrado
     * @throws DocumentException Excepcion que se lanza cuando ocurre una
     * excepcion de documento
     * @throws ListIsVoidException Excepcion que se lanza cuando la lista esta
     * vacia
     * @throws ListOutLimitException Excepcion que se lanza cuando la posicion
     * supera los limites de la lista
     */
    public abstract Boolean crearRegistro(String titulo) throws FileNotFoundException, DocumentException, ListIsVoidException, ListOutLimitException;

    /**
     * Método que retona la fecha en un formato dd-MM-yyyy
     *
     * @return Retorna un string que contiene la fecha
     */
    protected String getFecha() {
        Date fecha = new Date(Calendar.getInstance().getTimeInMillis());
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(fecha);
    }
}
