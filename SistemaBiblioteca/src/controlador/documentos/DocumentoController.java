/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.documentos;

import vistas.FrDatosLibro;
import controlador.documentos.excepciones.FechaNoValida;
import controlador.listas.ListaEnlazada;
import controlador.listas.NodoLista;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import java.awt.TextField;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.EjemplaresLibro;
import modelo.EjemplaresRevista;
import modelo.Libro;
import modelo.Revista;
import util.Utilidades;
import vistas.models.ModeloTablaLibro;

/**
 *
 * @author Usuario
 */
public class DocumentoController {

    private ListaEnlazada<Libro> libroList;
    private ListaEnlazada<Revista> revistaList;
    private Libro libro;
    private ListaEnlazada<EjemplaresLibro> ejemplaresLibro;
    private ListaEnlazada<EjemplaresRevista> ejemplaresRevista;

    /**
     *
     * @param edicion eidicon
     * @param lugarPublicacion lugar publicacion
     * @param idioma idioma
     * @param titulo titulo
     * @param editorial editorial
     * @param autor autor
     * @param paginas paginas
     * @param anioPublicacion anio publicacion
     * @param nro_ejemplares nro ejemplares
     * @return retorna libro
     *
     * public Libro insertarLibro(Integer edicion, String lugarPublicacion,
     * String idioma, String titulo, String editorial, String autor, Integer
     * paginas, LocalDate anioPublicacion, Integer nro_ejemplares) {
     * EjemplaresLibro auxEjemplaresLibro = new
     * EjemplaresLibro(getLibroList().getSize() + 1, nro_ejemplares); Libro
     * auxLibro = new Libro(edicion, lugarPublicacion, getLibroList().getSize()
     * + 1, idioma, titulo, editorial, autor, paginas, anioPublicacion);
     * getLibroList().insertar(auxLibro);
     * getEjemplaresLibro().insertar(auxEjemplaresLibro); return auxLibro; }
     *
     * /**
     *
     *
     * @param volumen volumen
     * @param isbn isbn
     * @param idioma idioma
     * @param titulo titulo
     * @param editorial editorial
     * @param autor autor
     * @param paginas paginas
     * @param anioPublicacion anio publicacion
     * @param nro_ejemplares nro ejemplares
     * @return retorna una revista
     *
     * Inserta una revista en la lista de revistas
     *
     */
    public Revista insertarRevista(Integer volumen, String isbn, String idioma,
            String titulo, String editorial, String autor, Integer paginas, LocalDate anioPublicacion, Integer nro_ejemplares) {
        EjemplaresRevista auxEjemplaresRevista = new EjemplaresRevista(getRevistaList().getSize() + 1, nro_ejemplares);
        Revista auxRevista = new Revista(volumen, isbn, anioPublicacion, getRevistaList().getSize() + 1, idioma, titulo, editorial, autor, paginas, anioPublicacion);
        getRevistaList().insertar(auxRevista);
        getEjemplaresRevista().insertar(auxEjemplaresRevista);
        return auxRevista;
    }

    /**
     * Muestra la informacion de un libro existente en la lista de libros
     *
     * @param id usado para obtener la posicion del libro dentro de la lista
     */
    public void mostrarLibro(Integer id) {
        FrDatosLibro datosLibro = new FrDatosLibro();
        libroList = (ListaEnlazada<Libro>) Utilidades.cargarJson(datosLibro.getDc().getClass(), "Listalibro");
        try {
            libro.setTitulo(libroList.obtener(id).getTitulo());
            libro.setAutor(libroList.obtener(id).getAutor());
            libro.setIdioma(libroList.obtener(id).getIdioma());
            libro.setEditorial(libroList.obtener(id).getEditorial());
            libro.setPaginas(libroList.obtener(id).getPaginas());
            libro.setEdicion(libroList.obtener(id).getEdicion());
            libro.setAnioPublicacion(libroList.obtener(id).getAnioPublicacion());
            libro.setLugarPublicacion(libroList.obtener(id).getLugarPublicacion());
             

        
        } catch (ListIsVoidException ex) {
            Logger.getLogger(DocumentoController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ListOutLimitException ex) {
            Logger.getLogger(DocumentoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



/**
 * Busca un libros existentes en la lista, recibiendo el atributo de este, y el
 * dato a buscar
 *
 * @param atributo del libro en el cual se evaluara el dato
 * @param dato a buscar
 * @return lista de libros
 * @throws Exception
 */
public ListaEnlazada buscarLibro(String atributo, String dato) throws Exception{
        ListaEnlazada<Libro> lista=new ListaEnlazada<>();

        if (atributo.equalsIgnoreCase("lugarPublicacion")) {
            lista = libroList.busquedaLineal(new Libro(null, dato, null, null, null, null, null, null, null), false, "lugarPublicacion");
        } else if (atributo.equalsIgnoreCase("ID")) {
            for (int i = 0; i < libroList.getSize(); i++) {
                Libro l = libroList.obtener(i);
                if (l.getId() == Integer.valueOf(dato)) {
                    lista.insertar(l);
                }
            }
        } else if (atributo.equalsIgnoreCase("Idioma")) {
            for (int i = 0; i < libroList.getSize(); i++) {
                Libro l = libroList.obtener(i);
                if (l.getIdioma().contains(dato)) {
                    lista.insertar(l);
                }
            }
        } else if (atributo.equalsIgnoreCase("Titulo")) {
            for (int i = 0; i < libroList.getSize(); i++) {
                Libro l = libroList.obtener(i);
                if (l.getTitulo().contains(dato)) {
                    lista.insertar(l);
                }
            }
        } else if (atributo.equalsIgnoreCase("Autor")) {
            for (int i = 0; i < libroList.getSize(); i++) {
                Libro l = libroList.obtener(i);
                if (l.getAutor().contains(dato)) {
                    lista.insertar(l);
                }
            }
        } else if (atributo.equalsIgnoreCase("Editorial")) {
            for (int i = 0; i < libroList.getSize(); i++) {
                Libro l = libroList.obtener(i);
                if (l.getEditorial().contains(dato)) {
                    lista.insertar(l);
                }
            }
        }

        return lista;
    }

    
    /**
     * Busca una revista que contemga el dato enviado
     * 
     * @param atributo de la revista para buscar
     * @param dato a buscar
     * @return lista de revistas
     * @throws Exception 
     */
    public ListaEnlazada buscarRevisa(String atributo, String dato) throws Exception{
        ListaEnlazada<Revista> lista=new ListaEnlazada<>();

        if (atributo.equalsIgnoreCase("ISBN")) {
            lista = revistaList.busquedaLineal(new Revista(null, dato, null, null, null, null, null, null, null, null), false, "ISBN");
        } else if (atributo.equalsIgnoreCase("ID")) {
            for (int i = 0; i < revistaList.getSize(); i++) {
                Revista r = revistaList.obtener(i);
                if (r.getId() == Integer.valueOf(dato)) {
                    lista.insertar(r);
                }
            }
        } else if (atributo.equalsIgnoreCase("Titulo")) {
            for (int i = 0; i < revistaList.getSize(); i++) {
                Revista r = revistaList.obtener(i);
                if (r.getTitulo().contains(dato)) {
                    lista.insertar(r);
                }
            }
        } else if (atributo.equalsIgnoreCase("Autor")) {
            for (int i = 0; i < revistaList.getSize(); i++) {
                Revista r = revistaList.obtener(i);
                if (r.getAutor().contains(dato)) {
                    lista.insertar(r);
                }
            }
        } else if (atributo.equalsIgnoreCase("Editorial")) {
            for (int i = 0; i < revistaList.getSize(); i++) {
                Revista r = revistaList.obtener(i);
                if (r.getEditorial().contains(dato)) {
                    lista.insertar(r);
                }
            }
        }

        return lista;
    }

    public ListaEnlazada<Libro> getLibroList() {
        if (libroList == null) {
            libroList = new ListaEnlazada<>();
        }
        return libroList;
    }

    public void setLibroList(ListaEnlazada<Libro> libroList) {
        this.libroList = libroList;
    }

    public ListaEnlazada<Revista> getRevistaList() {
        if (revistaList == null) {
            revistaList = new ListaEnlazada<>();
        }
        return revistaList;
    }

    public void setRevistaList(ListaEnlazada<Revista> revistaList) {
        this.revistaList = revistaList;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public ListaEnlazada<EjemplaresLibro> getEjemplaresLibro() {
        if (ejemplaresLibro == null) {
            ejemplaresLibro = new ListaEnlazada<>();
        }
        return ejemplaresLibro;
    }

    public void setEjemplaresLibro(ListaEnlazada<EjemplaresLibro> ejemplaresLibro) {
        this.ejemplaresLibro = ejemplaresLibro;
    }

    public ListaEnlazada<EjemplaresRevista> getEjemplaresRevista() {
        if (ejemplaresRevista == null) {
            ejemplaresRevista = new ListaEnlazada<>();
        }
        return ejemplaresRevista;
    }

    public void setEjemplaresRevista(ListaEnlazada<EjemplaresRevista> ejemplaresRevista) {
        this.ejemplaresRevista = ejemplaresRevista;
    }

    public void verificarFecha(String fecha) throws FechaNoValida {

    }

}
