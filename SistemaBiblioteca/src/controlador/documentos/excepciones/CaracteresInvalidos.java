/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.documentos.excepciones;

/**
 *
 * @author Usuario
 */
public class CaracteresInvalidos extends Exception{

    public CaracteresInvalidos() {
        super("No permite caracateres Invalidos");
    }

    public CaracteresInvalidos(String message) {
        super(message);
    }
    
}
