/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.documentos.excepciones;

/**
 *
 * @author Usuario
 */
public class IDInvalido extends Exception{

    public IDInvalido() {
        super("ID invalido, verifique si el ID ingresado ya pertenece a otro documento");
    }

    public IDInvalido(String message) {
        super(message);
    }
    
}
