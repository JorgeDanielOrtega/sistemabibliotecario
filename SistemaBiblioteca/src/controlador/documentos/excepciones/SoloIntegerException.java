/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.documentos.excepciones;

/**
 *
 * @author Usuario
 */
public class SoloIntegerException extends Exception {

    public SoloIntegerException() {
        super("Solo se admiten valores enteros");
    }

    public SoloIntegerException(String message) {
        super(message);
    }
    
}
