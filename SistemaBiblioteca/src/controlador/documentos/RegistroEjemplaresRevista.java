/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.documentos;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import controlador.Registro;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import modelo.EjemplaresRevista;
import modelo.Revista;

/**
 *
 * @author Usuario
 */
public class RegistroEjemplaresRevista extends Registro{
    ListaEnlazada<EjemplaresRevista> listaEjemRev;

    /**
     * Constructor con un parametro
     *
     * @param nombreArchivo Nombre de como se quiere que se guarde el archivo
     * pdf
     */
    public RegistroEjemplaresRevista(String nombreArchivo) {
        super(nombreArchivo);
    }

    /**
     * Constructor con dos parametros
     *
     * @param listaEjemRev Lista de empleados a representar en el pdf
     * @param nombreArchivo Nombre de como se quiere que se guarde el archivo
     * pdf
     */
    public RegistroEjemplaresRevista(ListaEnlazada<EjemplaresRevista> listaEjemRev, String nombreArchivo) {
        super(nombreArchivo);
        this.listaEjemRev = listaEjemRev;
    }

    /**
     * Crea en registro en pdf
     *
     * @param titulo Titulo que contendra el pdf
     * @return Retorna true si todo a saliido bien
     * @throws FileNotFoundException Excepcion que se lanza cuando el archivo no
     * ha sido encontrado
     * @throws DocumentException Excepcion que se lanza cuando ocurre una
     * excepcion de documento
     * @throws ListIsVoidException Excepcion que se lanza cuando la lista esta
     * vacia
     * @throws ListOutLimitException Excepcion que se lanza cuando la posicion
     * supera los limites de la lista
     */
    @Override
    public Boolean crearRegistro(String titulo) throws FileNotFoundException, DocumentException, ListIsVoidException, ListOutLimitException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(fullpath));
        document.open();

        Font fuenteTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
        Font fuenteDescripcion = new Font(Font.FontFamily.COURIER, 12, Font.ITALIC);

        Paragraph tituloText = new Paragraph(titulo, fuenteTitulo);
        tituloText.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);

        Paragraph descripcion = new Paragraph("\n" + "Creado el: " + getFecha() + "\n\n", fuenteDescripcion);

        List l = new List(false);
        for (int i = 0; i < listaEjemRev.getSize(); i++) {
            Paragraph item = new Paragraph(listaEjemRev.obtener(i).presentar());
            l.add(item.toString());
        }

        document.add(tituloText);
        document.add(descripcion);
        document.add(l);
        document.close();

        System.out.println("pdf creado");
        return true;
    }

    public ListaEnlazada<EjemplaresRevista> getListaEjemRev() {
        return listaEjemRev;
    }

    public void setListaEjemRev(ListaEnlazada<EjemplaresRevista> listaEjemRev) {
        this.listaEjemRev = listaEjemRev;
    }
    
    
    
}
