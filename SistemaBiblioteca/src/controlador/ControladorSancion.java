/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.listas.ListaEnlazada;
import modelo.DocumentoBibliografico;
import modelo.Persona;
import modelo.Prestamo;
import modelo.Sancion;

/**
 *
 * @author cobos
 */
public class ControladorSancion {
     public static final transient String NOMBRE_ARCHIVO = "sanciones";
    private ListaEnlazada<Sancion> sancionLista = new ListaEnlazada<>();

    public ControladorSancion() {

    }

    public void insertarNuevaSancion(Integer diasSancion, Integer diasRestante, Object prestamo) {
        Sancion aux = new Sancion(sancionLista.getSize() + 1, diasSancion, diasRestante,(Prestamo) prestamo);
        System.out.println(aux);
        getSancionLista().insertar(aux);
    }

    public void buscar(Integer tipoBusqueda, String elemento, String atributo) {
        try {
            Integer id = null;
            Integer dias = null;
            Persona persona = null;
            DocumentoBibliografico documentoBibliografico = null;
            
            if (atributo.equalsIgnoreCase("id")) {
                id = Integer.valueOf(elemento);
            }else if(atributo.equalsIgnoreCase("dias")){
                dias = Integer.valueOf(elemento);
            }
//            }else if (atributo.equalsIgnoreCase("persona")){
//                persona = elemento;
//            }else if (atributo.equalsIgnoreCase("documentoBibliografico")){
//                documentoBibliografico = DocumentoBibliografico.valueOf(elemento.toUpperCase());
//            }
            ListaEnlazada<Sancion> aux = new ListaEnlazada<>();
            if (tipoBusqueda == 1) {
                aux.insertar((Sancion) this.getSancionLista().busquedaBinaria(new Prestamo( id, dias,  persona, documentoBibliografico), false, atributo));
            } else {
                ListaEnlazada<Object> auxObject = getSancionLista().busquedaBinariaLineal(new Prestamo( id, dias,  persona, documentoBibliografico), false, atributo);
                for (int i = 0; i < auxObject.getSize(); i++) {
                    aux.insertar((Sancion) auxObject.obtener(i));
                }
            }
            setSancionLista(aux);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public ListaEnlazada<Sancion> getSancionLista() {
        return sancionLista;
    }

    public void setSancionLista(ListaEnlazada<Sancion> sancionLista) {
        this.sancionLista = sancionLista;
    }


}
