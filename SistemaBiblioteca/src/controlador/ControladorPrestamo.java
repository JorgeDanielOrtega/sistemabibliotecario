/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.listas.ListaEnlazada;
import modelo.DocumentoBibliografico;
import modelo.Persona;
import modelo.Prestamo;

/**
 *
 * @author cobos
 */
public class ControladorPrestamo {
    public static final transient String NOMBRE_ARCHIVO = "prestamos";
    private ListaEnlazada<Prestamo> prestamoLista = new ListaEnlazada<>();

    public ControladorPrestamo() {

    }

    public void insertarNuevoPrestamo(Integer dias, Object persona, Object documentoBibliografico) {
        Prestamo aux = new Prestamo(prestamoLista.getSize() + 1, dias, (Persona) persona, (DocumentoBibliografico) documentoBibliografico);
        System.out.println(aux);
        getPrestamoLista().insertar(aux);
    }

    public void buscar(Integer tipoBusqueda, String elemento, String atributo) {
        try {
            Integer id = null;
            Integer dias = null;
            Persona persona = null;
            DocumentoBibliografico documentoBibliografico = null;
            
            if (atributo.equalsIgnoreCase("id")) {
                id = Integer.valueOf(elemento);
            }else if(atributo.equalsIgnoreCase("dias")){
                dias = Integer.valueOf(elemento);
            }
//           }else if (atributo.equalsIgnoreCase("persona")){
//                persona = persona.setNombres(elemento);
//            }else if (atributo.equalsIgnoreCase("documentoBibliografico")){
//                documentoBibliografi;
//            }
            ListaEnlazada<Prestamo> aux = new ListaEnlazada<>();
            if (tipoBusqueda == 1) {
                aux.insertar((Prestamo) this.getPrestamoLista().busquedaBinaria(new Prestamo( id, dias, new Persona(persona.getNombres(), persona.getApellidos(), persona.getCedula()), documentoBibliografico), false, atributo));
            } else {
                ListaEnlazada<Object> auxObject = getPrestamoLista().busquedaBinariaLineal(new Prestamo( id, dias,  persona, documentoBibliografico), false, atributo);
                for (int i = 0; i < auxObject.getSize(); i++) {
                    aux.insertar((Prestamo) auxObject.obtener(i));
                }
            }
            setPrestamoLista(aux);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

    }

    public ListaEnlazada<Prestamo> getPrestamoLista() {
        return prestamoLista;
    }

    public void setPrestamoLista(ListaEnlazada<Prestamo> prestamoLista) {
        this.prestamoLista = prestamoLista;
    }

   
}
