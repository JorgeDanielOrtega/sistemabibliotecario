/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.excepciones.ClaveIncorrectoExcepcion;
import controlador.excepciones.UsuarioIncorrectoExcepcion;
import controlador.listas.ListaEnlazada;
import modelo.Cuenta;
import util.FileJSON;

/**
 * Clase controlador del Login
 *
 * @author daniel
 */
public class ControladorLogin {

    private transient static final String RUTA_ARCHIVO = "cuentas";
    private transient static final Integer NRO_INTENTOS_MAX = 3;
    private transient static final Integer NRO_INTENTOS_MIN = 0;
    private transient static final Integer ID_ROL_BIBLIOTECARIO = 1;
    private transient static final Integer NRO_CARACTERES_MAX = 3;
    private ListaEnlazada<Cuenta> cuentaList;
    private Cuenta cuenta;
    private Integer intentos = NRO_INTENTOS_MAX;

    /**
     * Constructor sin parametros, carga la lista de las cuentas mediante el
     * archivo cuentas.json
     */
    public ControladorLogin() {
        cuentaList = new FileJSON(RUTA_ARCHIVO).cargar(ControladorCuenta.class).getCuentaList(); //Quizas de problemas porque no es mi modulo
    }

    /**
     * Valida la clave del usuario
     *
     * @param clave Clave a evaluar
     * @return Retorna un true, en caso se haya validado correctamente la clave,
     * por el contrario, devolverá un false
     * @throws ClaveIncorrectoExcepcion Excepción lanzada en caso se haya
     * ingresado mal la clave
     */
    private Boolean validarClave(String clave) throws ClaveIncorrectoExcepcion {
        Boolean validacion = false;
        if (clave.equals(this.cuenta.desencriptarClave())) {
            return validacion = true;
        } else if (intentos > NRO_INTENTOS_MIN) {
            throw new ClaveIncorrectoExcepcion(--intentos);
        }
        return validacion;
    }

    /**
     * Valida el usuario ingresado
     *
     * @param usuario Nombre del usuario perteneciente de la cuenta
     * @return Retorna un true, en caso se haya validado la existencia del
     * usuario, por el contrario, devolverá un false
     * @throws UsuarioIncorrectoExcepcion Excepción lanzada cuando se haya
     * ingresado mal el usuario
     * @throws Exception Excepción general
     */
    private Boolean validarUsuario(String usuario) throws UsuarioIncorrectoExcepcion, Exception {
        cuenta = null;
//        NodoLista<Cuenta> auxiliar = cuentaList.getCabecera();
//        while (auxiliar != null) {
//            if (auxiliar.getDato().getUsuario().equals(usuario)) {
//                cuenta = auxiliar.getDato();
//                return Boolean.TRUE;
//            } else {
//                auxiliar = auxiliar.getSiguiente();
//            }
//        }
        cuenta = (Cuenta) cuentaList.busquedaBinaria(new Cuenta(null, usuario, null, null, null), false, "usuario");
        if (cuenta != null) {
            return true;
        } else {
            resetearIntentos(usuario);
            throw new UsuarioIncorrectoExcepcion();
        }
    }

    /**
     * Resetea los intentos para escribir la clave
     *
     * @param usuario Usuario enviado como parametro para validar el usuario
     * ingresado con el guardado temporalmente
     */
    private void resetearIntentos(String usuario) {
        if (cuenta != null) {
            if (cuenta.getUsuario() != usuario) {
                intentos = NRO_INTENTOS_MAX;
            }
        }
    }

    /**
     * Inicia la sesion una vez se haya confirmado el usuario y clave de la
     * cuenta
     *
     * @param usuario Usuario ingresado perteneciente de la cuenta
     * @param clave Clave de la cuenta
     * @return Retorna un true, en caso el inicio de sesion haya sido
     * satisfactorio, por el contrario, devolverá un false
     * @throws UsuarioIncorrectoExcepcion Excepción lanzada cuando se haya
     * ingresado mal el usuario
     * @throws ClaveIncorrectoExcepcion Excepción lanzada en caso se haya
     * ingresado mal la clave
     * @throws Exception Excepción general
     */
    public Boolean iniciarSesion(String usuario, String clave) throws UsuarioIncorrectoExcepcion, ClaveIncorrectoExcepcion, Exception {
        if (validarUsuario(usuario) && validarClave(clave) && cuenta.getEstaActiva() == true) {
            Sesion.getInstance().setCuenta(cuenta);
            return true;
        }
        return false;
    }

    /**
     * Valida si los intentos de ingreso de clave se han acabado
     *
     * @return Retorna true, en caso se hayan acabado los intentos, por el
     * contrario, devolverá false
     */
    public Boolean intentosAcabados() {
        return intentos == NRO_INTENTOS_MIN;
    }

    /**
     * Valida si la cuenta le pertenece a una persona con el rol bibliotecario
     *
     * @return Retorna true, en caso la cuenta le pertenzca a una persona con el
     * rol bibliotecario, por el contrario, devolverá false
     */
    public Boolean isBibliotecario() {
        return cuenta.getPersona().getRol().getId() == ID_ROL_BIBLIOTECARIO;
    }

    /**
     * Retorna una parte de la clave a modo de ayuda
     *
     * @return Retorna la clave fraccionada en caso el usuario ingresado exista,
     * por el contrario, devolverá un String vacio
     */

    public String retornarAyudaClave() {
        if (cuenta != null) {
            String clave = cuenta.desencriptarClave();
            return "Su clave empieza por " + clave.substring(0, NRO_CARACTERES_MAX) + "...";
        }
        return "";
    }

    public ListaEnlazada<Cuenta> getCuentaList() {
        if (cuentaList == null) {
            cuentaList = new ListaEnlazada<>();
        }
        return cuentaList;
    }

    public void setCuentaList(ListaEnlazada<Cuenta> cuentaList) {
        this.cuentaList = cuentaList;
    }

}
