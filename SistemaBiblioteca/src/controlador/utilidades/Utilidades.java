/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.utilidades;

import java.lang.reflect.Field;
import javax.swing.JComboBox;

/**
 *
 * @author cobos
 */
public class Utilidades {
    
    private static final Integer A_MINUSCULA = 97;
    private static final Integer Z_MINUSCULA = 122;
    private static final Integer A_MAYUSCULA = 65;
    private static final Integer Z_MAYUSCULA = 90;
    private static final Integer ESPACIO = 32;

    public static Boolean isNumber(Class clase) {
        return clase.getSuperclass().getSimpleName().equalsIgnoreCase("Number");
    }

    public static Boolean isString(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("String");
    }

    public static Boolean isCharacter(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("Character");
    }

    public static Boolean isBoolean(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("Boolean");
    }

    public static Boolean isPrimitive(Class clase) {
        return clase.isPrimitive();
    }

    public static Boolean isObject(Class clase) {
        return (!isBoolean(clase) && !isCharacter(clase)
                && !isNumber(clase) && !isString(clase) && !isPrimitive(clase)
                && !isEnum(clase));
    }

    public static Boolean isEnum(Class clase) {
        return clase.getSuperclass().getSimpleName().toLowerCase().contains("enum");
    }

    public static Field obtenerAtributo(Class clase, String nombre) {
        Field atributo = null;
        for (Field aux : clase.getDeclaredFields()) {
            if (aux.getName().equalsIgnoreCase(nombre)) {
                atributo = aux;
                break;
            }
        }
        return atributo;
    }

    public static Boolean hayCaracteresNoValidos(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (!((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA)
                        || posicionAscii == ESPACIO)) {
                    return true;
                }
            }
        }
        return false;
    }

}
