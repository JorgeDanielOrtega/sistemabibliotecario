/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.listas.ListaEnlazada;
import modelo.Cuenta;
import modelo.Persona;
import vistas.exceptions.ElementoNoEncontradoExcepcion;

/**
 * Clase controlador para la Cuenta
 *
 * @author daniel
 */
public class ControladorCuenta {

    public static final transient String NOMBRE_ARCHIVO = "cuentas";
    private transient Cuenta cuentaTemp;
    private ListaEnlazada<Cuenta> cuentaList;

    /**
     * Constructor vacio por defecto
     */
    public ControladorCuenta() {

    }

    /**
     * Inserta una cuenta en la lista a partir de los datos enviados
     *
     * @param usuario Nombre del usuario de la cuenta
     * @param clave Clave del usuario de la cuenta
     * @param persona Datos de la persona de la cuenta
     * @return Retorna un true como confirmación de la insercción de la cuenta
     */
    public Boolean insertarCuenta(String usuario, String clave, Persona persona) {
        Cuenta aux = new Cuenta(getCuentaList().getSize() + 1, usuario, clave, Boolean.TRUE, persona);
        getCuentaList().insertar(aux);
        return true;
    }

    /**
     * Buscar una cuenta por el atributo y el elemento a buscar
     *
     * @param atributo Atributo de la cuenta a buscar
     * @param elemento Valor del atributo
     * @return Retorna una lista de cuentas en caso existan coincidencias, en
     * caso contrario, retorna una lista vacia
     * @throws ElementoNoEncontradoExcepcion Excepcion que es lanzada cuando el
     * elemento no se puede encontrar
     * @throws Exception Excepcion general
     */
    public ListaEnlazada<Cuenta> buscarCuenta(String atributo, String elemento) throws ElementoNoEncontradoExcepcion, Exception {
        //considerando que la busqueda de una cuenta se hara solo con un id y con el usuario

        Integer id = null;
        String usuario = null;
        String clave = null;
        Persona persona = null;

        if (atributo.equalsIgnoreCase("id")) {
            id = Integer.valueOf(elemento);
            System.out.println(id);
        } else if (atributo.equalsIgnoreCase("usuario")) {
            usuario = elemento;
        }
        if (atributo.equalsIgnoreCase("id")) {
            System.out.println(id);
            Cuenta c = (Cuenta) cuentaList.busquedaBinaria(new Cuenta(id, usuario, clave, null, persona), false, atributo.toLowerCase());
            if (c == null) {
                throw new ElementoNoEncontradoExcepcion();
            } else {
                cuentaList.vaciar();
                cuentaList.insertar(c);
            }
        } else {
            cuentaList = cuentaList.busquedaLineal(new Cuenta(id, usuario, clave, null, persona), false, atributo.toLowerCase());
        }
        if (cuentaList.getSize() == 0 || cuentaList == null) {
            throw new ElementoNoEncontradoExcepcion();
        }
        return cuentaList;
    }

    /**
     * Método que se encargar de modificar la clave de una cuenta segun el id
     *
     * @param id Id de la cuenta a modificar
     * @param nuevaClave La nueva clave a reemplazar
     * @throws Exception Excepcion general
     * @throws ElementoNoEncontradoExcepcion Excepcion que se lanza cuando un
     * elemento no se encuentre
     */
    public void modificarClave(Integer id, String nuevaClave) throws Exception, ElementoNoEncontradoExcepcion {
        Integer pos = (Integer) cuentaList.busquedaBinaria(new Cuenta(id, null, null, null, null), Boolean.TRUE, "id");
        if (pos == null) {
            throw new ElementoNoEncontradoExcepcion("El elemento no se encontro");
        } else {
            System.out.println("pos no es nulo");
            Cuenta cTemp = cuentaList.obtener(pos);
            System.out.println("cTemp");
            cTemp.setClave(nuevaClave);
        }

    }

    /**
     * Método que verifica si el nombre del usurio ya existe
     *
     * @param usuario Usuario a buscar
     * @return Retorna true, si el nombre del usuario ya existe, en caso
     * contrario, retorna false
     * @throws ElementoNoEncontradoExcepcion Excepcion que es lanzada cuando el
     * elemento no se puede encontrar
     * @throws Exception Excepcion general
     */
    public Boolean existeUsuario(String usuario) throws ElementoNoEncontradoExcepcion, Exception {
        Cuenta c = (Cuenta) cuentaList.busquedaBinaria(new Cuenta(null, usuario, null, null, null), false, "usuario");
        System.out.println("la cuenta rescatada");
        System.out.println(c);
        return c != null;
    }

    /**
     * Método que se encarga de eliminar la cuenta de vinculada a una persona,
     * la cual va a ser borrada
     *
     * @param posPersona Posicion de la persona en la lista
     * @param controladorPersona Controlador de la persona
     * @return Retornar true, si el proceso fue exitoso, en caso contrario,
     * retorna false
     * @throws Exception Excepcion general
     */
    public Boolean eliminarCuentaPorPersona(Integer posPersona, ControladorPersona controladorPersona) throws Exception { //TODO en un futuro, mejorar los algoritmos para buscar objetos internos
        Persona p = controladorPersona.getPersonaList().obtener(posPersona);
        for (int i = 0; i < getCuentaList().getSize(); i++) {
            Cuenta c = getCuentaList().obtener(i);
            if (c.getPersona().getId().equals(p.getId())) {
                getCuentaList().eliminarPosicion(i);
                return true;
            }
        }
        return false;
    }

    /**
     * Método que se encarga de modificar la persona de una cuenta,
     * la cual fue modificada
     *
     * @param posPersona Posicion de la persona en la lista
     * @param controladorPersona Controlador de la persona
     * @return Retornar true, si el proceso fue exitoso, en caso contrario,
     * retorna false
     * @throws Exception Excepcion general
     */
    public Boolean modificarPersonaPorPosicion(Integer posPersona, ControladorPersona controladorPersona) throws Exception { //TODO en un futuro, mejorar los algoritmos para buscar objetos internos
        Persona p = controladorPersona.getPersonaList().obtener(posPersona);
        for (int i = 0; i < getCuentaList().getSize(); i++) {
            Cuenta c = getCuentaList().obtener(i);
            if (c.getPersona().getId().equals(p.getId())) {
                c.setPersona(p);
                return true;
            }
        }
        return false;
    }

    public ListaEnlazada<Cuenta> getCuentaList() {
        if (cuentaList == null) {
            cuentaList = new ListaEnlazada<>();
        }
        return cuentaList;
    }

    public void setCuentaList(ListaEnlazada<Cuenta> cuentaList) {
        this.cuentaList = cuentaList;
    }

    public Cuenta getCuentaTemp() {
        return cuentaTemp;
    }

    public void setCuentaTemp(Cuenta cuentaTemp) {
        this.cuentaTemp = cuentaTemp;
    }

}
