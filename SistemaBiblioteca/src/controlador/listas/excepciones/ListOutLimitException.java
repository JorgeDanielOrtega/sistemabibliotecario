/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.listas.excepciones;

/**
 * Clase excepción para la superación de los límites de la lista
 *
 * @author daniel
 */
public class ListOutLimitException extends Exception {

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public ListOutLimitException(String msg) {
        super(msg);
    }

    /**
     * Constructor vacio que contiene el mesaje por defecto, mostrando la
     * posición inválida
     * @param pos posicion
     */
    public ListOutLimitException(Integer pos) {
        super("La posicion: " + pos + " sobrepasa los limites de la lista");
    }

}
