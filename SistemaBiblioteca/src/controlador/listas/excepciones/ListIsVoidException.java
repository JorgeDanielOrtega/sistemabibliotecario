/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.listas.excepciones;

/**
 * Clase excepción para la lista vacia
 *
 * @author daniel
 */
public class ListIsVoidException extends Exception {

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public ListIsVoidException(String msg) {
        super(msg);
    }

    /**
     * Constructor vacio que contiene el mesaje por defecto "La lista esta
     * vacia"
     */
    public ListIsVoidException() {
        super("La lista esta vacia");
    }
}
