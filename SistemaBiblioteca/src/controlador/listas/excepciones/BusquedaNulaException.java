/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.listas.excepciones;

/**

 * Clase excepción para la busqueda nula
 *
 * @author daniel
 */
public class BusquedaNulaException extends Exception {

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public BusquedaNulaException(String msg) {
        super(msg);
    }

}
