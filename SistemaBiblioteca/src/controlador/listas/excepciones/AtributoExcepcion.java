/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.listas.excepciones;

/**
 * Clase excepción para el atributo
 *
 * @author daniel
 */
public class AtributoExcepcion extends Exception {

    /**
     * Constructor vacio que contiene el mesaje por defecto "No se puede
     * encontrar el atributo dado"
     */
    public AtributoExcepcion() {
        super("No se puede encontrar el atributo dado");
    }

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public AtributoExcepcion(String msg) {
        super(msg);
    }

}
