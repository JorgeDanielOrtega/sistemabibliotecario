/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.listas.ListaEnlazada;
import modelo.Rol;
import util.Utilidades;


/**
 * Clase controlador de roles
 * @author daniel
 */
public class ControladorRol {

    public static final transient String NOMBRE_ARCHIVO = "roles";
    private transient Rol rolTemp;
    private ListaEnlazada<Rol> rolList;

    /**
     * Controlador por defecto sin parametros
     */
    public ControladorRol() {
    }

    /**
     * Busca un rol por el nombre dado
     * @param nombre Nombre a buscar
     * @return Retorna el rol si se existe, por el contrario, retorna null
     */
    public Rol buscarRolPorNombre(String nombre) {
        try {
            System.out.println(nombre.toUpperCase());
            getRolList().print();
            Rol rolTemp = (Rol) getRolList().busquedaBinaria(new Rol(null, nombre.toUpperCase(), null), false, "nombre");
            return rolTemp;

        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
    /**
     * metodo para registrar un nuevo rol
     * @param nombre nombre del rol
     * @param descripcion detalle sobre la funcion que este rol ejerce
     * @return retorna un boolean de confirmacion
     */
    public Boolean insertarRol(String nombre, String descripcion) {
        Rol rolTemp = new Rol(getRolList().getSize() + 1, nombre.toUpperCase(), Utilidades.capitalizarPrimeraPalabra(nombre));
        getRolList().insertar(rolTemp);
        return true;
    }
    public ListaEnlazada<Rol> getRolList() {
        if (rolList == null) {
            rolList = new ListaEnlazada<>();
        }
        return rolList;
    }

    public void setRolList(ListaEnlazada<Rol> rolList) {
        this.rolList = rolList;
    }

    public Rol getRolTemp() {
        return rolTemp;
    }

    public void setRolTemp(Rol rolTemp) {
        this.rolTemp = rolTemp;
    }

}
