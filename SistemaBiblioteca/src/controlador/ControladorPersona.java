/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.listas.ListaEnlazada;
import modelo.Persona;
import modelo.Rol;
import util.Utilidades;
import vistas.exceptions.ElementoNoEncontradoExcepcion;

/**
 * Clase controlador de Persona
 *
 * @author daniel
 */
public class ControladorPersona {

    public static final transient String NOMBRE_ARCHIVO = "personas";
    private transient Persona personaTemp;
    private transient ControladorRol controladorRol = new ControladorRol();
    private ListaEnlazada<Persona> personaList;

    /**
     * Constructor vacio por defecto
     */
    public ControladorPersona() {
    }

    /**
     * Busca a las personas a partir de un atributo de la clase y su valor
     *
     * @param atributo Atributo de la clase Persona por la cual se va a buscar
     * @param elementoBusqueda Valor del atributo de la clase Persona
     * @return Retorna una lista de las personas que coinciden con el valor del
     * atributo especificado
     * @throws ElementoNoEncontradoExcepcion Exepción que se lanza en el caso no
     * se haya encontrado ninguna coincidencia
     * @throws Exception Excepción general
     */
    public ListaEnlazada<Persona> buscarPersona(String atributo, String elementoBusqueda) throws ElementoNoEncontradoExcepcion, Exception {

        Integer id = null;
        String nombres = null;
        String apellidos = null;
        Rol rol = null;
        String cedula = null;

        if (atributo.equalsIgnoreCase("id")) {
            id = Integer.valueOf(elementoBusqueda);
            System.out.println(id);
        } else if (atributo.equalsIgnoreCase("nombres")) {
            nombres = elementoBusqueda;
        } else if (atributo.equalsIgnoreCase("apellidos")) {
            apellidos = elementoBusqueda;
        } else if (atributo.equalsIgnoreCase("rol")) {
            controladorRol = (ControladorRol) Utilidades.cargarJson(controladorRol.getClass(), controladorRol.NOMBRE_ARCHIVO);
            rol = controladorRol.buscarRolPorNombre(elementoBusqueda);
            if (rol == null) {
                throw new ElementoNoEncontradoExcepcion();
            }
        } else if (atributo.equalsIgnoreCase("cedula")) {
            cedula = elementoBusqueda;
        }

        if (atributo.equalsIgnoreCase("id")) {
            System.out.println(id);
            Persona p = (Persona) personaList.busquedaBinaria(new Persona(id, nombres, apellidos, cedula, null, rol), false, atributo.toLowerCase());
            if (p == null) {
                throw new ElementoNoEncontradoExcepcion();
            } else {

                personaList.vaciar();
                personaList.insertar(p);
            }

        } else {
            personaList = personaList.busquedaLineal(new Persona(id, nombres, apellidos, cedula, null, rol), false, atributo.toLowerCase());
        }
        if (personaList.getSize() == 0 || personaList == null) {
            throw new ElementoNoEncontradoExcepcion();
        }
        return personaList;
    }

    /**
     * Inserta una persona en la lista de personas mediante los datos enviados
     *
     * @param nombre Nombre de la persona
     * @param apellido Apellido de la persona
     * @param cedula Cédula de la persona
     * @param rol Rol de la persona
     * @return Retorna el objeto Persona creado a partir de los datos enviados
     */
    public Persona insertarPersona(String nombre, String apellido, String cedula, Rol rol) {
        Persona personaAux = new Persona(getPersonaList().getSize() + 1, nombre, apellido, cedula, Boolean.TRUE, rol);
        getPersonaList().insertar(personaAux);
        return personaAux;
    }

    /**
     * Marca como null el valor del atributo Rol con el que se encuentra
     * coincidencia en la lista de personas
     *
     * @param posRol Posicion del rol dentro de la lista de roles
     * @param controladorRol Controlador del rol
     * @return Retornar true cuando se haya acabado el proceso de puesta a nulos
     * @throws Exception Excepción general
     */
    public Boolean marcarNullRol(Integer posRol, ControladorRol controladorRol) throws Exception {
        Rol r = controladorRol.getRolList().obtener(posRol);
        for (int i = 0; i < getPersonaList().getSize(); i++) {
            Persona p = getPersonaList().obtener(i);
            if (p.getRol().getId().equals(r.getId())) {
                p.setRol(null);
            }
        }
        return true;
    }

    /**
     * Elimina a la persona de la lista de personas mediante un id
     *
     * @param posCuenta Posicion de la cuenta dentro de la lista de cuentas
     * @param controladorCuenta Controlador de la cuenta
     * @return Retorna true si se elimino la persona correctamente, por el
     * contrario, retorna false si ocurrio un error o no se encontro dicha
     * persona
     * @throws Exception Excepción general
     */
    public Boolean eliminarPersonaPorId(Integer posCuenta, ControladorCuenta controladorCuenta) throws Exception {
        Integer personaId = controladorCuenta.getCuentaList().obtener(posCuenta).getPersona().getId();
        for (int i = 0; i < getPersonaList().getSize(); i++) {
            Persona p = getPersonaList().obtener(i);
            if (p.getId().equals(personaId)) {
                getPersonaList().eliminarPosicion(i);
                return true;
            }
        }
        return false;
    }

    /**
     * Modifica el rol de la persona
     *
     * @param posRol Posicion del rol dentro de la lista de roles
     * @param controladorRol Controlador del rol
     * @return Retorna true en caso se haya modificado el rol correctamente, por
     * el contrario, retorna false, en caso no se haya encontrado el rol o haya
     * ocurrido un error
     * @throws Exception excepcion
     */
    public Boolean modificarRolPorPosicion(Integer posRol, ControladorRol controladorRol) throws Exception {
        Rol r = controladorRol.getRolList().obtener(posRol);
        for (int i = 0; i < getPersonaList().getSize(); i++) {
            Persona p = getPersonaList().obtener(i);
            if (p.getRol().getId().equals(r.getId())) {
                p.setRol(r);
                return true;
            }
        }
        return false;
    }

    public ListaEnlazada<Persona> getPersonaList() {
        if (personaList == null) {
            personaList = new ListaEnlazada<>();
        }
        return personaList;
    }

    public void setPersonaList(ListaEnlazada<Persona> cuentaList) {
        this.personaList = cuentaList;
    }

    public Persona getPersonaTemp() {
        return personaTemp;
    }

    public void setPersonaTemp(Persona personaTemp) {
        this.personaTemp = personaTemp;
    }

}
