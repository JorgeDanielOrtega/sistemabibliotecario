/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.models;

import vistas.models.*;
import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Sancion;

/**
 *
 * @author cobos
 */
public class ModeloTablaSancion extends AbstractTableModel {
     private ListaEnlazada<Sancion> lista = new ListaEnlazada<>();

    public ListaEnlazada<Sancion> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Sancion> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "Dias Sancion";
            case 2:
                return "Dias Restantes";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Sancion p = null;
        try {
            p = lista.obtener(rowIndex);
        } catch (Exception e) {
        }
        switch (columnIndex) {
            case 0:
                return (rowIndex + 1);
            case 1:
                return (p != null) ? p.getDiasSancion().toString() : "NO DEFINIDO";
            case 2:
                return (p != null) ? p.getDiasRestantes().toString() : "NO DEFINIDO";
            default:
                return null;
        }
    }

}
