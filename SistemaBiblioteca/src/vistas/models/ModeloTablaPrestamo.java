/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.models;

import vistas.models.*;
import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Prestamo;

/**
 *
 * @author cobos
 */
public class ModeloTablaPrestamo extends AbstractTableModel{
     private ListaEnlazada<Prestamo> lista = new ListaEnlazada<>();

    public ListaEnlazada<Prestamo> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Prestamo> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "Dias";
            case 2:
                return "Cliente";
            case 3:
                return "Documento Bibliografico";
            case 4:
                return "Fecha Prestamo";
            case 5:
                return "Fecha Devolucion";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Prestamo p = null;
        try {
            p = lista.obtener(rowIndex);
            System.out.println(p.getPersona() == null);
        } catch (Exception e) {
        }
        switch (columnIndex) {
            case 0:
                return (rowIndex + 1);
            case 1:
                return (p != null) ? p.getDias().toString() : "NO DEFINIDO";
            case 2:
                return (p != null) ? p.getPersona().getNombres() : "NO DEFINIDO";
            case 3:
                return (p != null) ? p.getDocumentoBibliografico().getTitulo() : "NO DEFINIDO";
            case 4:
                return (p != null) ? p.getFechaPrestamo() : "NO DEFINIDO";
            case 5:
               return (p != null) ? p.getFechaDevolucion() : "NO DEFINIDO";
            default:
                return null;
        }
    }
}
