/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas.models;

import vistas.models.*;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import modelo.Libro;
import modelo.Revista;

/**
 *
 * @author Usuario
 */
public class ModeloTablaRevista extends AbstractTableModel {
    ListaEnlazada<Revista> listaRevista=new ListaEnlazada<>();

    public ListaEnlazada<Revista> getListaRevista() {
        return listaRevista;
    }

    public void setListaRevista(ListaEnlazada<Revista> listaRevista) {
        this.listaRevista = listaRevista;
    }
    
    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return listaRevista.getSize();
    }

    @Override
    public String getColumnName(int i) {
        
        switch(i) {
            case 0: return "ID";
            case 1: return "Titulo";
            case 2: return "Autor";
            case 3: return "ISBN";
            case 4: return "Mes Publicacion";
           
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int i, int i1) {
       
        try {
            Revista e= listaRevista.obtener(i);
            switch(i1) {
                case 0: return (e != null) ? e.getId() : "NO DEFINIDO";
                case 1: return (e != null) ? e.getTitulo(): "NO DEFINIDO";
                case 2: return (e != null) ? e.getAutor() : "NO DEFINIDO";
                case 3: return (e != null) ? e.getIsbn() : "NO DEFINIDO";
                case 4: return (e != null) ? e.getMesPublicacion().getMonth() : "NO DEFINIDO";
                default: return null;
            }
            
        } catch (ListIsVoidException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } catch (ListOutLimitException ex) {
JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return null;
    }
 
}
