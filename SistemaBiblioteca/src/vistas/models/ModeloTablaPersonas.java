/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.models;

import vistas.models.*;
import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Persona;

/**
 * Modelo de la tabla de empleados
 * @author daniel
 */
public class ModeloTablaPersonas extends AbstractTableModel {

    private ListaEnlazada<Persona> lista = new ListaEnlazada<>();

    public ListaEnlazada<Persona> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Persona> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Id";
            case 1:
                return "Nombres";
            case 2:
                return "Apellidos";
            case 3:
                return "Cedula";
            case 4:
                return "Estado";
            case 5:
                return "Rol";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Persona persona = null;
        try {
            persona = lista.obtener(rowIndex);
        } catch (Exception e) {
        }
        switch (columnIndex) {
            case 0:
                return (persona != null) ? persona.getId().toString() : "NO DEFINIDO";
            case 1:
                return (persona != null) ? persona.getNombres() : "NO DEFINIDO";
            case 2:
                return (persona != null) ? persona.getApellidos() : "NO DEFINIDO";
            case 3:
                return (persona != null) ? persona.getCedula() : "NO DEFINIDO";
            case 4:
                return (persona != null) ? persona.getEstado().toString() : "NO DEFINIDO";
            case 5:
                return (persona != null) ? persona.getRol().getNombre().toUpperCase() : "NO DEFINIDO";
            default:
                return null;
        }

    }

}
