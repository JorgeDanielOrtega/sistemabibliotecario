
package vistas.models;

import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Rol;

/**
 *
 * @author Leo
 */
public class TablaRol extends AbstractTableModel{
    
    private ListaEnlazada <Rol> listaRol;
    
    public ListaEnlazada<Rol> getListRol(){
        return listaRol;
    }
    public void setListRol(ListaEnlazada <Rol> listaRol){
        this.listaRol = listaRol;
    }
    
    @Override
    public int getRowCount() {
        return listaRol.getSize();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        try{
            Rol rol = listaRol.obtener(rowIndex);
            
            switch(columnIndex){
                case 0:
                    return(rol != null)? rol.getId():"Sin Definir";
                case 1:
                    return(rol != null)? rol.getNombre():"Sin Definir";
                case 2:
                    return(rol != null)? rol.getDescripcion():"Sin Definir";
                default:
            }
        }catch(Exception e ){
            System.out.println("Error en tabla");
        }
        return null;
    }
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "ID";
            case 1:
                return "Nombre de Rol";
            case 2:
                return "Descripcion";
            default:
                return null;

        }
    }
    
}
