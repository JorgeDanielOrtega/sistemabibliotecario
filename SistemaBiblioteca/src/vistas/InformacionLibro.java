/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package vistas;

import vistas.*;
import controlador.documentos.DocumentoController;
import controlador.documentos.excepciones.CaracteresInvalidos;
import controlador.documentos.excepciones.SoloIntegerException;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.DocumentoBibliografico;
import modelo.Libro;
import util.Utilidades;
import vistas.AgregarDocumento;
import vistas.exceptions.CamposVaciosExcepcion;

/**
 *
 * @author Usuario
 */
public class InformacionLibro extends javax.swing.JFrame {

    private AgregarDocumento agregarDocumento;
    FrDatosLibro datosLibro = new FrDatosLibro();
    DocumentoController dc = (DocumentoController) Utilidades.cargarJson(datosLibro.getDc().getClass(), "Listalibro");
    Integer pos = Integer.parseInt(agregarDocumento.txtBuscar.getText());
    DateTimeFormatter formato = DateTimeFormatter.ofPattern("d-MM-yyyy");

    /**
     * Creates new form InformacionLibro
     */
    public InformacionLibro() {
        initComponents();
        setLocationRelativeTo(null);
        //if (this.isVisible()) {
        cargarInfLibro(Integer.parseInt(agregarDocumento.txtBuscar.getText()) - 1);

        //}
    }

    /**
     * Verifica los campos para la actualizacion de un Libro
     *
     * @return true
     * @throws SoloIntegerException
     * @throws CaracteresInvalidos
     */
    private Boolean verificarCampos() throws SoloIntegerException, CaracteresInvalidos {
        if (!Utilidades.haySoloNumeros(txtPagLibro.getText())) {
            throw new SoloIntegerException("La pagina del libro debe ser un numero");
        } else if (!Utilidades.haySoloNumeros(txtEdiLibro.getText())) {
            throw new SoloIntegerException("La edicion del libro debe ser un numero");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtAutorLibro.getText())) {
            throw new CaracteresInvalidos("El Autor del libro no debe tener caracteres especiales");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtTituloLibro.getText())) {
            throw new CaracteresInvalidos("El Titulo del libro no debe tener caracteres especiales");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtIdiomaLibro.getText())) {
            throw new CaracteresInvalidos("El Idioma del libro no debe tener caracteres especiales");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtLugarLibro.getText())) {
            throw new CaracteresInvalidos("El Lugar de publicacion del libro no debe tener caracteres especiales");
        }
        return true;
    }

    /**
     * Evalua que cada uno de los campos no esten vacios
     * 
     * @return True si no existen campos vacios, false si hay alguno
     * @throws CamposVaciosExcepcion 
     */
    private Boolean camposVacios() throws CamposVaciosExcepcion {
        if (txtTituloLibro.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("Titulo no puede estar vacio");
        } else if (txtPagLibro.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("Pagina de Libro no puede estar vacio");
        } else if (txtEdiLibro.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("La edicion del Libro no puede estar vacio");
        } else if (txtAutorLibro.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("Autor del Libro no puede estar vacio");
        } else if (txtIdiomaLibro.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("Idioma del Libro no puede estar vacio");
        } else if (txtLugarLibro.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("Lugar de publicacion del Libro no puede estar vacio");
        }
        return true;
    }

    /**
     * Carga la informacion de un Libro existente en la lista de Libros, a
     * partir de su ubicacion
     *
     * @param posicion Usada para obtener el libro a partir de la lista de
     * Libros
     */
    public void cargarInfLibro(Integer posicion) {
        try {

            txtTituloLibro.setText(dc.getLibroList().obtener(posicion).getTitulo());
            txtAutorLibro.setText(dc.getLibroList().obtener(posicion).getAutor());
            txtIdiomaLibro.setText(dc.getLibroList().obtener(posicion).getIdioma());
            txtEditorialLibro.setText(dc.getLibroList().obtener(posicion).getEditorial());
            txtPagLibro.setText(dc.getLibroList().obtener(posicion).getPaginas().toString());
            txtEdiLibro.setText(dc.getLibroList().obtener(posicion).getEdicion().toString());
            txtAnioLibro.setText(dc.getLibroList().obtener(posicion).getAnioPublicacion().format(formato));
            txtLugarLibro.setText(dc.getLibroList().obtener(posicion).getLugarPublicacion());
        } catch (ListIsVoidException ex) {
            Logger.getLogger(InformacionLibro.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ListOutLimitException ex) {
            Logger.getLogger(InformacionLibro.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtTituloLibro = new javax.swing.JTextField();
        txtAutorLibro = new javax.swing.JTextField();
        txtIdiomaLibro = new javax.swing.JTextField();
        txtEditorialLibro = new javax.swing.JTextField();
        txtPagLibro = new javax.swing.JTextField();
        txtAnioLibro = new javax.swing.JTextField();
        txtEdiLibro = new javax.swing.JTextField();
        txtLugarLibro = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setMinimumSize(new java.awt.Dimension(550, 350));
        jPanel1.setPreferredSize(new java.awt.Dimension(400, 350));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setText("Titulo:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 37, -1));

        jLabel3.setText("Autor:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

        jLabel4.setText("Idioma:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        jLabel5.setText("Editorial:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        jLabel7.setText("Año Publicacion:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, -1, -1));

        jLabel6.setText("Paginas:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, -1, -1));

        jLabel8.setText("Edicion:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 180, -1, -1));

        jLabel9.setText("Lugar Publicacion:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, -1, -1));

        txtTituloLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTituloLibroActionPerformed(evt);
            }
        });
        txtTituloLibro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTituloLibroKeyTyped(evt);
            }
        });
        jPanel1.add(txtTituloLibro, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 320, -1));
        jPanel1.add(txtAutorLibro, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, 320, -1));
        jPanel1.add(txtIdiomaLibro, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, 320, -1));
        jPanel1.add(txtEditorialLibro, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 150, 320, -1));
        jPanel1.add(txtPagLibro, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 180, 120, -1));

        txtAnioLibro.setText("DD-MM-AAAA");
        txtAnioLibro.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtAnioLibro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtAnioLibroMousePressed(evt);
            }
        });
        txtAnioLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAnioLibroActionPerformed(evt);
            }
        });
        jPanel1.add(txtAnioLibro, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 220, 270, -1));
        jPanel1.add(txtEdiLibro, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 180, 140, -1));
        jPanel1.add(txtLugarLibro, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 260, 270, -1));

        jButton1.setText("Actualizar");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 310, -1, -1));

        jButton2.setText("Eliminar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 310, -1, -1));

        jButton3.setText("Atras");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 310, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 502, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 350, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtTituloLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTituloLibroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTituloLibroActionPerformed

    private void txtTituloLibroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTituloLibroKeyTyped
        // TODO add your handling code here:
        //        Character c = new Character(c);
        //        if(c.)
    }//GEN-LAST:event_txtTituloLibroKeyTyped

    private void txtAnioLibroMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtAnioLibroMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAnioLibroMousePressed

    private void txtAnioLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAnioLibroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAnioLibroActionPerformed

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked


    }//GEN-LAST:event_jButton1MouseClicked
    Libro libro = new Libro();
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        LocalDate fecha = LocalDate.parse(txtAnioLibro.getText(), formato);
        try {
            if (camposVacios()) {
                if (verificarCampos()) {
                    if (fecha.isBefore(LocalDate.now())) {
                        libro.setId(pos);
                        libro.setTitulo(txtTituloLibro.getText());
                        libro.setAutor(txtAutorLibro.getText());
                        libro.setIdioma(txtIdiomaLibro.getText());
                        libro.setEditorial(txtEditorialLibro.getText());
                        libro.setPaginas(Integer.parseInt(txtPagLibro.getText()));
                        libro.setEdicion(Integer.parseInt(txtEdiLibro.getText()));
                        libro.setAnioPublicacion(fecha);
                        libro.setLugarPublicacion(txtLugarLibro.getText());
                        dc.getLibroList().modificarPosicion(libro, pos - 1);
                        Utilidades.guardarJson(dc, "Listalibro");
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "La fecha ingresada no puede ser una fecha futura", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        } catch (ListIsVoidException | ListOutLimitException | SoloIntegerException | CaracteresInvalidos | CamposVaciosExcepcion ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            // TODO add your handling code here:
            dc.getLibroList().eliminarPosicion(pos - 1);
            dc.getEjemplaresLibro().eliminarPosicion(pos - 1);
            dc.getLibroList().print();
            Utilidades.guardarJson(dc, "Listalibro");
            dispose();
        } catch (ListIsVoidException ex) {
            Logger.getLogger(InformacionLibro.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ListOutLimitException ex) {
            Logger.getLogger(InformacionLibro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InformacionLibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InformacionLibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InformacionLibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InformacionLibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InformacionLibro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtAnioLibro;
    private javax.swing.JTextField txtAutorLibro;
    private javax.swing.JTextField txtEdiLibro;
    private javax.swing.JTextField txtEditorialLibro;
    private javax.swing.JTextField txtIdiomaLibro;
    private javax.swing.JTextField txtLugarLibro;
    private javax.swing.JTextField txtPagLibro;
    private javax.swing.JTextField txtTituloLibro;
    // End of variables declaration//GEN-END:variables
}
