package vistas;

import controlador.ControladorCuenta;
import controlador.ControladorPersona;
import controlador.RegistroCuenta;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import util.Utilidades;
import vistas.exceptions.ContenidoInvalidoExcepcion;
import vistas.models.TablaCuenta;

/**
 *
 * @author Leo117
 */
public class VistaAdminCuentas extends javax.swing.JDialog {

    
    private TablaCuenta tabla = new TablaCuenta();
    ControladorPersona controlP = new ControladorPersona();

    ControladorCuenta controlC = new ControladorCuenta();
    Integer fila = -1;

    public VistaAdminCuentas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cargarDatosCuenta();
        btnElegirCuenta.setEnabled(false);
        controlC = (ControladorCuenta) Utilidades.cargarJson(ControladorCuenta.class, ControladorCuenta.NOMBRE_ARCHIVO);
        cargarTabla();
        cargarCombo();
        cargarListeners();

    }
    /**
     * Metodo para cargar los eventos que se requieren para la seleccion un datos de la tabla mostrada
     */
    private void cargarListeners() {
        //TODO quizas meter las lineas redundantes en una funcion

        MouseListener m = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                resetTabla();
                requestFocus();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        };

        FocusListener f = new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if (e.getSource() == TablaCuenta) {
                    System.out.println("foco");
                    btnElegirCuenta.setEnabled(true);
                    //btnEliminar.setEnabled(true);
                } else if (e.getSource() == txtBusqueda) {
                    resetTabla();
                    txtBusqueda.requestFocus();
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
            }

        };

        ActionListener a = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == btonBuscar) {
                    resetTabla();
                    requestFocus();
                }
            }
        };

        this.addMouseListener(m);
        TablaCuenta.addFocusListener(f);
        txtBusqueda.addFocusListener(f);
        btonBuscar.addActionListener(a);
    }
    /**
     * metodo para actualizar los datos que se muestran en la tabla de cuentas
     */
    private void refresh() {
        cargarDatosCuenta();
        cargarTabla();
    }

    public ControladorPersona getControlP() {
        return controlP;
    }

    public void setControlP(ControladorPersona controlP) {
        this.controlP = controlP;
    }

    public ControladorCuenta getControlC() {
        return controlC;
    }

    public void setControlC(ControladorCuenta controlC) {
        this.controlC = controlC;
    }
    /**
     * metodo para agregar los atributos que se usan para la busqueda de una cuenta
     */
    private void cargarCombo() {
        Utilidades.cargarComboAtributosCuenta(cbxAtributo);
    }
    /**
     * metodo para quitar la seleccion previa de la cuenta dentro de la tabla
     */
    private void resetTabla() {
        TablaCuenta.clearSelection();
    }
    /**
     * metodo para poder mostrar en la tabla las cuesntas que estan registradas
     */
    private void cargarTabla() {
        tabla.setListCuenta(controlC.getCuentaList());
        TablaCuenta.setModel(tabla);
        tabla.fireTableDataChanged();
    }
    /**
     * Cargar los datos de las cuentan que estan registradas
     */
    private void cargarDatosCuenta() {
        controlC = (ControladorCuenta) Utilidades.cargarJson(ControladorCuenta.class, ControladorCuenta.NOMBRE_ARCHIVO);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnElegirCuenta = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cbxAtributo = new javax.swing.JComboBox<>();
        txtBusqueda = new javax.swing.JTextField();
        btonBuscar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        TablaCuenta = new javax.swing.JTable();
        btonRefrescar = new javax.swing.JButton();
        btonRegistro = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnElegirCuenta.setText("Seleccionar");
        btnElegirCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnElegirCuentaActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setText("Buscar por:");

        cbxAtributo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ID", "Usuario" }));

        btonBuscar.setText("Buscar");
        btonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btonBuscar)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbxAtributo, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cbxAtributo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(btonBuscar)
                .addContainerGap())
        );

        TablaCuenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Usuario", "Clave", "Estado"
            }
        ));
        jScrollPane2.setViewportView(TablaCuenta);

        btonRefrescar.setText("Refrescar");
        btonRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonRefrescarActionPerformed(evt);
            }
        });

        btonRegistro.setText("Registro");
        btonRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonRegistroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 37, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 393, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnElegirCuenta)
                                .addGap(72, 72, 72)
                                .addComponent(btonRegistro)
                                .addGap(78, 78, 78)
                                .addComponent(btonRefrescar)))))
                .addGap(40, 40, 40))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnElegirCuenta)
                    .addComponent(btonRefrescar)
                    .addComponent(btonRegistro))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * bton para cargar los datos de la cuenta seleccionada para llevarlos a la vista de la modificacion de cuenta
     * @param evt 
     */
    private void btnElegirCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnElegirCuentaActionPerformed
        try {
            ModificarCuenta md = new ModificarCuenta(null, true);
            md.setControladorCuenta(controlC);
            md.asignarCuenta(TablaCuenta.getSelectedRow());
            md.setVisible(true);
            controlC.modificarPersonaPorPosicion(TablaCuenta.getSelectedRow(), controlP);
            cargarTabla();
            Utilidades.guardarJson(controlC, "cuentas");

        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnElegirCuentaActionPerformed
    /**
     * Boton para iniciar la busqueda de una cuenta por medio de un atributo
     * @param evt  boton de Busqueda
     */
    private void btonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonBuscarActionPerformed
        // TODO add your handling code here:
        try {
            if (verificarCampoBusqueda()) {
                controlC.buscarCuenta((String) cbxAtributo.getSelectedItem(), txtBusqueda.getText().trim());
                cargarTabla();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            refresh();
            btonBuscar.setEnabled(true);
        }
        txtBusqueda.setText("");
        btonBuscar.setEnabled(false);
    }//GEN-LAST:event_btonBuscarActionPerformed
    /**
     * Ejecuta el refrescado de la tabla de cuentas
     * @param evt 
     */
    private void btonRefrescarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonRefrescarActionPerformed
        // TODO add your handling code here:
        refresh();
        txtBusqueda.setText("");
        btonBuscar.setEnabled(true);
    }//GEN-LAST:event_btonRefrescarActionPerformed
    /**
     * boton para generar el registro en formato PDF  de las cuentas que se encuentran ingresadas
     * @param evt 
     */
    private void btonRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonRegistroActionPerformed
        // TODO add your handling code here:
        try {
            RegistroCuenta registroCuenta = new RegistroCuenta(controlC.getCuentaList(),"cuentas");
            registroCuenta.crearRegistro("Registro de Cuentas");
            JOptionPane.showMessageDialog(this, "Registro creado con exito","Confirmacion",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btonRegistroActionPerformed
    /**
     * Metodo para validar el dato que se ingresa sea correcta para su poder usarlo en la busqueda de una cuenta
     * @return retorna true en caso de que el valor ingresado en valido
     * caso contrario false
     * @throws ContenidoInvalidoExcepcion Excepcion generada en caso de no cumplir con las restricciones y condificones establecidas
     */

    private Boolean verificarCampoBusqueda() throws ContenidoInvalidoExcepcion {
        String texto = txtBusqueda.getText().trim();
        if (texto.equals("")) {
            JOptionPane.showMessageDialog(null, new ContenidoInvalidoExcepcion("El campo esta vacio"));
        } else if (((String) cbxAtributo.getSelectedItem()).equalsIgnoreCase("id")) {
            if (!Utilidades.haySoloNumeros(texto) || texto.toCharArray().length > 9) {
                JOptionPane.showMessageDialog(null, new ContenidoInvalidoExcepcion("Solo pueden hacer numeros enteros y menores a 9 "));
            } else if (Utilidades.hayCaracteresNoValidos(texto)) {
                JOptionPane.showMessageDialog(null, new ContenidoInvalidoExcepcion("Solo se permiten letras"));
            } else if (Utilidades.hayEspaciosJuntos(texto)) {
                JOptionPane.showMessageDialog(null, new ContenidoInvalidoExcepcion("Existen Espacios Juntos "));
            } else if (!Utilidades.hayNPalabras(texto, 2) && !Utilidades.hayNPalabras(texto, 1)) {
                JOptionPane.showMessageDialog(null, "Campos no validos ");
            }
        }
        return true;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaAdminCuentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaAdminCuentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaAdminCuentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaAdminCuentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VistaAdminCuentas dialog = new VistaAdminCuentas(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TablaCuenta;
    private javax.swing.JButton btnElegirCuenta;
    private javax.swing.JButton btonBuscar;
    private javax.swing.JButton btonRefrescar;
    private javax.swing.JButton btonRegistro;
    private javax.swing.JComboBox<String> cbxAtributo;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
