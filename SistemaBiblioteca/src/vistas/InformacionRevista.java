/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package vistas;

import vistas.*;
import controlador.documentos.DocumentoController;
import controlador.documentos.excepciones.CaracteresInvalidos;
import controlador.documentos.excepciones.SoloIntegerException;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Revista;
import util.Utilidades;
import vistas.AgregarDocumento;
import vistas.exceptions.CamposVaciosExcepcion;

/**
 *
 * @author Usuario
 */
public class InformacionRevista extends javax.swing.JFrame {

    private AgregarDocumento agregarDocumento;
    FrDatosRevista datosRevista = new FrDatosRevista();
    DocumentoController dc = (DocumentoController) Utilidades.cargarJson(datosRevista.getDc().getClass(), "ListaRevista");
    Integer pos = Integer.parseInt(agregarDocumento.txtBuscar.getText());

    /**
     * Creates new form InformacionRevista
     */
    public InformacionRevista() {
        initComponents();
        setLocationRelativeTo(null);

        cargarInfRevista(pos - 1);

    }

    /**
     * Carga la informacion de una revista seleccionado de la lista de Revistas
     *
     * @param pos posicion
     */
    public void cargarInfRevista(Integer pos) {
        try {
            txtIsbnRev.setText(dc.getRevistaList().obtener(pos).getIsbn());
            txtTituloRev.setText(dc.getRevistaList().obtener(pos).getTitulo());
            txtAutorRev.setText(dc.getRevistaList().obtener(pos).getAutor());
            txtIdiomaRev.setText(dc.getRevistaList().obtener(pos).getIdioma());
            txtEditorialRev.setText(dc.getRevistaList().obtener(pos).getEditorial());
            txtPagRev.setText(dc.getRevistaList().obtener(pos).getPaginas().toString());
            txtVolumenRev.setText(dc.getRevistaList().obtener(pos).getVolumen().toString());
            txtAnioRev.setText(dc.getRevistaList().obtener(pos).getAnioPublicacion().toString());

        } catch (ListIsVoidException | ListOutLimitException e) {
        }
    }

    /**
     * Evalua que cada uno de los campos no esten vacios
     * 
     * @return True si no existen campos vacios, false si hay alguno
     * @throws CamposVaciosExcepcion 
     */
    private Boolean verificarCampos() throws SoloIntegerException, CaracteresInvalidos {
        if (!Utilidades.haySoloNumeros(txtPagRev.getText())) {
            throw new SoloIntegerException("La pagina de la revista debe ser un numero");
        } else if (!Utilidades.haySoloNumeros(txtVolumenRev.getText())) {
            throw new SoloIntegerException("La edicion de la revista debe ser un numero");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtAutorRev.getText())) {
            throw new CaracteresInvalidos("El Autor de la revista no debe tener caracteres especiales");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtTituloRev.getText())) {
            throw new CaracteresInvalidos("El Titulo de la revista no debe tener caracteres especiales");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtIdiomaRev.getText())) {
            throw new CaracteresInvalidos("El Idioma de la revista no debe tener caracteres especiales");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtEditorialRev.getText())) {
            throw new CaracteresInvalidos("La editorial no debe tener caracteres especiales");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtIsbnRev.getText())) {
            throw new CaracteresInvalidos("El ISBN no debe tener caracteres especiales");
        }
        return true;
    }

    private Boolean camposVacios() throws CamposVaciosExcepcion {
        if (txtTituloRev.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("Titulo no puede estar vacio");
        } else if (txtPagRev.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("La pagina no puede estar vacio");
        } else if (txtIsbnRev.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("El ISBN no puede estar vacio");
        } else if (txtAutorRev.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("El autor no puede estar vacio");
        } else if (txtIdiomaRev.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("El idioma no puede estar vacio");
        } else if (txtEditorialRev.getText().isEmpty()) {
            throw new CamposVaciosExcepcion("La editorial no puede estar vacia");
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtTituloRev = new javax.swing.JTextField();
        txtAutorRev = new javax.swing.JTextField();
        txtIdiomaRev = new javax.swing.JTextField();
        txtEditorialRev = new javax.swing.JTextField();
        txtPagRev = new javax.swing.JTextField();
        txtAnioRev = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtVolumenRev = new javax.swing.JTextField();
        txtIsbnRev = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        btnEliminarRev = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setMinimumSize(new java.awt.Dimension(550, 350));
        jPanel1.setPreferredSize(new java.awt.Dimension(400, 350));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setText("Titulo:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 37, -1));

        jLabel3.setText("Autor:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

        jLabel4.setText("Idioma:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        jLabel5.setText("Editorial:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        jLabel7.setText("Fecha de Publicacion: ");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, -1, -1));

        jLabel6.setText("Paginas:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, -1, -1));

        txtTituloRev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTituloRevActionPerformed(evt);
            }
        });
        txtTituloRev.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTituloRevKeyTyped(evt);
            }
        });
        jPanel1.add(txtTituloRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 320, -1));
        jPanel1.add(txtAutorRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, 320, -1));
        jPanel1.add(txtIdiomaRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, 320, -1));
        jPanel1.add(txtEditorialRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 150, 320, -1));
        jPanel1.add(txtPagRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 180, 120, -1));

        txtAnioRev.setText("DD-MM-AAAA");
        txtAnioRev.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtAnioRevMousePressed(evt);
            }
        });
        jPanel1.add(txtAnioRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 220, 130, -1));

        jLabel1.setText("Volumen:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 180, -1, -1));

        jLabel11.setText("Isbn:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));
        jPanel1.add(txtVolumenRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 180, 120, -1));
        jPanel1.add(txtIsbnRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 30, 320, -1));

        jButton1.setText("Actualizar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 300, -1, -1));

        btnEliminarRev.setText("Eliminar");
        btnEliminarRev.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEliminarRevMouseClicked(evt);
            }
        });
        btnEliminarRev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarRevActionPerformed(evt);
            }
        });
        jPanel1.add(btnEliminarRev, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 300, -1, -1));

        jButton3.setText("Atras");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 300, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 350, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtTituloRevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTituloRevActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTituloRevActionPerformed

    private void txtTituloRevKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTituloRevKeyTyped
        // TODO add your handling code here:
        //        Character c = new Character(c);
        //        if(c.)
    }//GEN-LAST:event_txtTituloRevKeyTyped

    private void txtAnioRevMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtAnioRevMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAnioRevMousePressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnEliminarRevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarRevActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEliminarRevActionPerformed

    private void btnEliminarRevMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEliminarRevMouseClicked
        // TODO add your handling code here:
        try {
            dc.getRevistaList().eliminarPosicion(pos - 1);
            dc.getEjemplaresRevista().eliminarPosicion(pos - 1);
            dc.getRevistaList().print();
            Utilidades.guardarJson(dc, "ListaRevista");
            dispose();
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_btnEliminarRevMouseClicked
    Revista revista = new Revista();
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        DateTimeFormatter formato = DateTimeFormatter.ofPattern("d-MM-yyyy");
        LocalDate fecha = LocalDate.parse(txtAnioRev.getText(), formato);
        try {
            if (camposVacios()) {
                if (verificarCampos()) {
                    if (fecha.isBefore(LocalDate.now())) {
                        revista.setId(pos);
                        revista.setIsbn(txtIsbnRev.getText());
                        revista.setTitulo(txtTituloRev.getText());
                        revista.setAutor(txtAutorRev.getText());
                        revista.setVolumen(Integer.valueOf(txtVolumenRev.getText()));
                        revista.setMesPublicacion(fecha);
                        revista.setIdioma(txtIdiomaRev.getText());
                        revista.setEditorial(txtEditorialRev.getText());
                        revista.setPaginas(Integer.valueOf(txtPagRev.getText()));
                        dc.getRevistaList().modificarPosicion(revista, pos - 1);
                        Utilidades.guardarJson(dc, "ListaRevista");
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "La fecha ingresada no puede ser una fecha futura", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }

        } catch (ListIsVoidException | ListOutLimitException | SoloIntegerException | CaracteresInvalidos | CamposVaciosExcepcion ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InformacionRevista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InformacionRevista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InformacionRevista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InformacionRevista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InformacionRevista().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEliminarRev;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtAnioRev;
    private javax.swing.JTextField txtAutorRev;
    private javax.swing.JTextField txtEditorialRev;
    private javax.swing.JTextField txtIdiomaRev;
    private javax.swing.JTextField txtIsbnRev;
    private javax.swing.JTextField txtPagRev;
    private javax.swing.JTextField txtTituloRev;
    private javax.swing.JTextField txtVolumenRev;
    // End of variables declaration//GEN-END:variables
}
