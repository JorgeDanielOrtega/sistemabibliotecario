/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.exceptions;

import vistas.exceptions.*;

/**
 * Clase excepción para la clave no valida
 *
 * @author daniel
 */
public class ClaveNoValidaExcepcion extends Exception {

    /**
     * Constructor vacio
     */
    public ClaveNoValidaExcepcion() {
    }

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public ClaveNoValidaExcepcion(String msg) {
        super(msg);
    }

}
