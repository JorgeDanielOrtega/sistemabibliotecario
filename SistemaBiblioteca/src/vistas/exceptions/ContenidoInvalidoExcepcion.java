/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.exceptions;

import vistas.exceptions.*;

/**
 * Clase excepción para el contenido invalido
 * @author daniel
 */
public class ContenidoInvalidoExcepcion extends Exception {

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public ContenidoInvalidoExcepcion(String msg) {
        super(msg);
    }

}
