/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import vistas.*;
import controlador.ControladorCuenta;
import controlador.ControladorPersona;
import controlador.RegistroEmpleado;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import modelo.Cuenta;
import modelo.Persona;
import modelo.Rol;
import util.FileJSON;
import util.Utilidades;
import vistas.exceptions.ContenidoInvalidoExcepcion;
import vistas.models.ModeloTablaPersonas;

/**
 * Vista general para los empleados
 *
 * @author daniel
 */
public class FrmPersona extends javax.swing.JDialog {

    private ModeloTablaPersonas modeloTablaPersonas = new ModeloTablaPersonas();
    private ControladorPersona controladorPersona = new ControladorPersona();
    private ControladorCuenta controladorCuenta = new ControladorCuenta();

    /**
     * constructor
     *
     * @param parent padre
     * @param modal modal
     */
    public FrmPersona(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
//        controladorPersona = (ControladorPersona) Utilidades.cargarListaJson(ControladorPersona.class, ControladorPersona.NOMBRE_ARCHIVO);
        cargarDatosPersona();
        controladorCuenta = (ControladorCuenta) Utilidades.cargarJson(ControladorCuenta.class, ControladorCuenta.NOMBRE_ARCHIVO);
        cargarTabla();
        cargarCombo();
        inicializarBotones();
        cargarListeners();
        setLocationRelativeTo(null);
        this.setResizable(false);
    }

    /**
     * Cargar el JComboBox de persona con los atributos de la clase Persona
     */
    private void cargarCombo() {
        Utilidades.cargarComboAtributosPersona(cmbAtributos);
    }

    /**
     * Carga la tabla de los empleados
     */
    private void cargarTabla() {
        modeloTablaPersonas.setLista(controladorPersona.getPersonaList());
        tblEmpleados.setModel(modeloTablaPersonas);
        modeloTablaPersonas.fireTableDataChanged();
    }

    /**
     * Carga los datos de los empleados que se encuentren en el archivo
     * personas.json
     */
    private void cargarDatosPersona() {
        controladorPersona = (ControladorPersona) Utilidades.cargarJson(ControladorPersona.class, ControladorPersona.NOMBRE_ARCHIVO);
    }

    /**
     * Inicializa los botones de modificar y eliminar
     */
    private void inicializarBotones() {
        btnModificar.setEnabled(false);
        btnEliminar.setEnabled(false);
    }

    /**
     * Resetea el estado de la tabla, limpiando cualquier seleccion e
     * inicializando los botones de modificar y eliminar
     */
    private void resetearEstadoTabla() {
        inicializarBotones();
        tblEmpleados.clearSelection();
    }

    /**
     * Cargar los listeners pertenecientes a la vista
     */
    private void cargarListeners() {
        //TODO quizas meter las lineas redundantes en una funcion

        MouseListener m = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                resetearEstadoTabla();
                requestFocus();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        };

        FocusListener f = new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if (e.getSource() == tblEmpleados) {
                    System.out.println("foco");
                    btnModificar.setEnabled(true);
                    btnEliminar.setEnabled(true);
                } else if (e.getSource() == txtBusqueda) {
                    resetearEstadoTabla();
                    txtBusqueda.requestFocus();
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
            }

        };

        ActionListener a = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == btnBuscar) {
                    resetearEstadoTabla();
                    requestFocus();
                }
            }
        };

        this.addMouseListener(m);
        tblEmpleados.addFocusListener(f);
        txtBusqueda.addFocusListener(f);
        btnBuscar.addActionListener(a);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblEmpleados = new javax.swing.JTable();
        txtBusqueda = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        cmbAtributos = new javax.swing.JComboBox<>();
        btnReferescar = new javax.swing.JButton();
        btnCrearRegistro = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Administrar Cuentas");

        tblEmpleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblEmpleados);

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        cmbAtributos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnReferescar.setText("Refrescar");
        btnReferescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReferescarActionPerformed(evt);
            }
        });

        btnCrearRegistro.setText("Crear registro");
        btnCrearRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearRegistroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnReferescar)
                        .addGap(391, 391, 391)
                        .addComponent(cmbAtributos, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1007, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(btnCrearRegistro)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnModificar)
                            .addGap(18, 18, 18)
                            .addComponent(btnEliminar))))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar)
                    .addComponent(cmbAtributos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReferescar))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificar)
                    .addComponent(btnEliminar)
                    .addComponent(btnCrearRegistro))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
/**
     * Botón que elimina el empleado seleccionado
     *
     * @param evt Evento del botón
     */
    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        try {
            int res = JOptionPane.showConfirmDialog(this, "¿Está seguro de eliminar el empleado seleccionada?", "Eliminar Empleado", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            if (res == JOptionPane.OK_OPTION) {
                controladorCuenta.eliminarCuentaPorPersona(tblEmpleados.getSelectedRow(), controladorPersona);
                controladorPersona.getPersonaList().eliminarPosicion(tblEmpleados.getSelectedRow());
//                controladorCuenta.getCuentaList().print();
                controladorPersona.getPersonaList().print();
                cargarTabla();
                Utilidades.guardarJson(controladorPersona, "personas");
                Utilidades.guardarJson(controladorCuenta, "cuentas");
                JOptionPane.showMessageDialog(this, "Se ha eliminado el empleado correctamente", "Confirmacion", JOptionPane.INFORMATION_MESSAGE);
                resetearEstadoTabla();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnEliminarActionPerformed
    /**
     * Botón que carga la vista para modificar el empleado seleccionado
     *
     * @param evt Evento del botón
     */
    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        try {

            FrmModificarEmpleado frmModificarEmpleado = new FrmModificarEmpleado(null, true);
            frmModificarEmpleado.setControladorPersona(controladorPersona);
            frmModificarEmpleado.asignarEmpleado(tblEmpleados.getSelectedRow());
            frmModificarEmpleado.setVisible(true);
            controladorCuenta.modificarPersonaPorPosicion(tblEmpleados.getSelectedRow(), controladorPersona);
//            controladorCuenta.getCuentaList().print();            
            cargarTabla();
            resetearEstadoTabla();
            Utilidades.guardarJson(controladorPersona, "personas");
            Utilidades.guardarJson(controladorCuenta, "cuentas");
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnModificarActionPerformed
    /**
     * Verifica el campo de busqueda para asegurarse de que los valores
     * ingresados en este, cumplan con las restricciones impuestas
     *
     * @return Retorna true si el valor ingresado en el campo es valido, por el
     * contrario, retorna false
     * @throws ContenidoInvalidoExcepcion Excepción lanzada cuando el contenido
     * del campo de busqueda es inválido
     */
    private Boolean verificarCampoBusqueda() throws ContenidoInvalidoExcepcion {
        String texto = txtBusqueda.getText().trim();
        if (texto.equals("")) {
            throw new ContenidoInvalidoExcepcion("El campo esta vacio");
        } else if (((String) cmbAtributos.getSelectedItem()).equalsIgnoreCase("cedula")) {
            if (!Utilidades.haySoloNumeros(texto) || texto.toCharArray().length != 10) {
                throw new ContenidoInvalidoExcepcion("Por favor, escriba la cedula correctamente");
            }
        } else if (((String) cmbAtributos.getSelectedItem()).equalsIgnoreCase("id")) {
            if (!Utilidades.haySoloNumeros(texto) || texto.toCharArray().length > 9) {
                throw new ContenidoInvalidoExcepcion("Solo pueden haber numeros enteros y menores a 9 al buscar por id");
            }
        } else if (((String) cmbAtributos.getSelectedItem()).equalsIgnoreCase("rol")) {
            if (!Utilidades.haySoloLetrasConCharacterPersonalizado(texto, '_') || !Utilidades.hayNPalabras(texto, 1) || Utilidades.hayEspacios(texto)) {
                throw new ContenidoInvalidoExcepcion("Por favor, escriba correctamente el rol, sin espacios");
            }
        } else if (Utilidades.hayCaracteresNoValidos(texto)) {
            throw new ContenidoInvalidoExcepcion("Solo se permiten letras");
        } else if (Utilidades.hayEspaciosJuntos(texto)) {
            throw new ContenidoInvalidoExcepcion("No se permiten espacios juntos");
        } else if (!Utilidades.hayNPalabras(texto, 2) && !Utilidades.hayNPalabras(texto, 1)) {
            throw new ContenidoInvalidoExcepcion("Solo se puede escribir hasta dos nombres o dos apellidos ");
        }
        return true;
    }

    /**
     * Botón para empezar la búsqueda de la persona mediante los valores
     * proporcionados en el campo de búsqueda
     *
     * @param evt Evento del botón
     */
    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        try {
            if (verificarCampoBusqueda()) {
                controladorPersona.buscarPersona((String) cmbAtributos.getSelectedItem(), txtBusqueda.getText().trim());
                cargarTabla();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            refrescar();
        }
    }//GEN-LAST:event_btnBuscarActionPerformed
    /**
     * Refresca la tabla con los datos de las personas
     */
    private void refrescar() {
        cargarDatosPersona();
        cargarTabla();
    }

    /**
     * Ejecuta el refrescado de la tabla
     *
     * @param evt Evento del botón
     */
    private void btnReferescarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReferescarActionPerformed
        refrescar();
        txtBusqueda.setText("");
        cmbAtributos.setSelectedIndex(0);
    }//GEN-LAST:event_btnReferescarActionPerformed

    /**
     * Botón encargado de crear el registro de empleados en un pdf
     *
     * @param evt
     */
    private void btnCrearRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearRegistroActionPerformed
        try {
            RegistroEmpleado registroEmpleado = new RegistroEmpleado(controladorPersona.getPersonaList(), "personas");
            registroEmpleado.crearRegistro("Registro de empleados");
            JOptionPane.showMessageDialog(this, "El registro se creo con exito", "Confirmacion", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }//GEN-LAST:event_btnCrearRegistroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmPersona dialog = new FrmPersona(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCrearRegistro;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnReferescar;
    private javax.swing.JComboBox<String> cmbAtributos;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblEmpleados;
    private javax.swing.JTextField txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
