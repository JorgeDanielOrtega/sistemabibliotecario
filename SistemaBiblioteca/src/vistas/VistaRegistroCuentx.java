/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import vistas.*;
import controlador.ControladorCuenta;
import controlador.ControladorPersona;
import controlador.ControladorRol;
import controlador.excepciones.ClaveIncorrectoExcepcion;
import javax.swing.JOptionPane;
import util.Utilidades;
import vistas.exceptions.CamposVaciosExcepcion;
import vistas.exceptions.ContenidoInvalidoExcepcion;
import vistas.exceptions.ElementoNoEncontradoExcepcion;

/**
 *
 * @author daniel
 */
public class VistaRegistroCuentx extends javax.swing.JDialog {

    /**
     * Creates new form VistaRegistroCuentx
     */
    private ControladorCuenta controladorCuenta = new ControladorCuenta();
    private ControladorPersona controladorPersona = new ControladorPersona();
    private ControladorRol controladorRol = new ControladorRol();

    /**
     * Contructor padre
     *
     * @param parent padre
     * @param modal modal
     */
    public VistaRegistroCuentx(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        inicializarCampos();
        cargarCombo();
        controladorRol = (ControladorRol) Utilidades.cargarJson(controladorRol.getClass(), controladorRol.NOMBRE_ARCHIVO);
        controladorPersona = (ControladorPersona) Utilidades.cargarJson(controladorPersona.getClass(), controladorPersona.NOMBRE_ARCHIVO);
        controladorCuenta = (ControladorCuenta) Utilidades.cargarJson(controladorCuenta.getClass(), controladorCuenta.NOMBRE_ARCHIVO);

        setLocationRelativeTo(parent);
    }

    /**
     * Método que inicializa los campos nombre, apellido, cedula, clave,
     * confirmarClave y usuario del empleado a campos vacios
     */
    private void inicializarCampos() {
        txtNombreEmpleado.setText("");
        txtApellidoEmpleado.setText("");
        txtCedulaEmpleado.setText("");
        txtClave.setText("");
        txtConfirmarClave.setText("");
        txtUsuario.setText("");
    }

    /**
     * Método que se encarga de car cargar el combo de empleados con los roles
     * existentes
     */
    private void cargarCombo() {
        Utilidades.cargarCombo(cmbRolEmpleado);
    }

    /**
     * Método que se encarga de validar cedula ecuatoriana
     *
     * @return Retorna true, si la cedula es valida, en caso contrario, retorna
     * false
     * @throws CamposVaciosExcepcion Excepcion lanzada cuando los campos a
     * llenar están vacios
     * @throws ContenidoInvalidoExcepcion Excepcion lanzada cuando el contenido
     * de los campos es invalido
     */
    private Boolean validarCedula() throws CamposVaciosExcepcion, ContenidoInvalidoExcepcion {
        if (txtCedulaEmpleado.getText().trim().equals("")) {
            throw new CamposVaciosExcepcion();
        } else if (Utilidades.hayEspacios(txtCedulaEmpleado.getText())) {
            throw new ContenidoInvalidoExcepcion("No se permiten espacios en la cedula");
        } else if (!Utilidades.haySoloNumeros(txtCedulaEmpleado.getText())) {
            throw new ContenidoInvalidoExcepcion("Solo se permiten numeros en la cedula");
        } else if (txtCedulaEmpleado.getText().length() != 10) {
            throw new ContenidoInvalidoExcepcion("La cedula debe de tener 10 digitos");
        } else if (Utilidades.numerosIguales(txtCedulaEmpleado.getText().trim())) {
            throw new ContenidoInvalidoExcepcion("Los numeros de la cedula son iguales");
        }

        boolean cedulaCorrecta = false;

        try {

            if (txtCedulaEmpleado.getText().length() == 10) // ConstantesApp.LongitudCedula
            {
                int tercerDigito = Integer.parseInt(txtCedulaEmpleado.getText().substring(2, 3));
                if (tercerDigito < 6) {
                    int[] coefValCedula = {2, 1, 2, 1, 2, 1, 2, 1, 2};
                    int verificador = Integer.parseInt(txtCedulaEmpleado.getText().substring(9, 10));
                    int suma = 0;
                    int digito = 0;
                    for (int i = 0; i < (txtCedulaEmpleado.getText().length() - 1); i++) {
                        digito = Integer.parseInt(txtCedulaEmpleado.getText().substring(i, i + 1)) * coefValCedula[i];
                        suma += ((digito % 10) + (digito / 10));
                    }

                    if ((suma % 10 == 0) && (suma % 10 == verificador)) {
                        cedulaCorrecta = true;
                    } else if ((10 - (suma % 10)) == verificador) {
                        cedulaCorrecta = true;
                    } else {
                        cedulaCorrecta = false;
                    }
                } else {
                    cedulaCorrecta = false;
                }
            } else {
                cedulaCorrecta = false;
            }
        } catch (NumberFormatException nfe) {
            cedulaCorrecta = false;
        } catch (Exception err) {
            System.out.println("Una excepcion ocurrio en el proceso de validadcion");
            cedulaCorrecta = false;
        }

        if (!cedulaCorrecta) {
            throw new ContenidoInvalidoExcepcion("La Cédula ingresada es Incorrecta");
        }
        return cedulaCorrecta;
    }

    /**
     * Método que verifica los campos a llenar
     *
     * @return Retorna true, si los campos cumplen con las restricciones
     * programadas
     * @throws CamposVaciosExcepcion Excepcion lanzada cuando los campos a
     * llenar están vacios
     * @throws ContenidoInvalidoExcepcion Excepcion lanzada cuando el contenido
     * de los campos es invalido
     * @throws ElementoNoEncontradoExcepcion Excepcion que es lanzada cuando un
     * elemento no es encontrado
     * @throws Exception Excepcion general
     */
    private Boolean verificarCampos() throws CamposVaciosExcepcion, ContenidoInvalidoExcepcion, ElementoNoEncontradoExcepcion, Exception {

        if (txtUsuario.getText().trim().equals("")
                || String.valueOf(txtClave.getPassword()).trim().equals("")
                || String.valueOf(txtConfirmarClave.getPassword()).trim().equals("")
                || txtNombreEmpleado.getText().trim().equals("")
                || txtApellidoEmpleado.getText().trim().equals("")) {
            throw new CamposVaciosExcepcion();
        } else if (!Utilidades.hayNPalabras(txtNombreEmpleado.getText().trim(), 2) && !Utilidades.hayNPalabras(txtApellidoEmpleado.getText().trim(), 2)) {
            throw new ContenidoInvalidoExcepcion("Ingrese sus dos nombres y sus dos apellidos");
        } else if (Utilidades.hayEspacios(txtUsuario.getText())
                || Utilidades.hayEspacios((String.valueOf(txtClave.getPassword())))
                || Utilidades.hayEspacios(String.valueOf(txtConfirmarClave.getPassword()))) {
            throw new ContenidoInvalidoExcepcion("No se permiten espacios en el campo usuario, contraseña o confirmar contraseña");
        } else if (Utilidades.hayCaracteresNoValidosExcepcionNumeros(txtUsuario.getText())) {
            throw new ContenidoInvalidoExcepcion("Solo se puede ingresar letras o numeros en le campo usuario");
        } else if (Utilidades.haySoloNumeros(txtUsuario.getText())) {
            throw new ContenidoInvalidoExcepcion("No puede haber solo numeros en el campo usuario");
        } else if (controladorCuenta.existeUsuario(txtUsuario.getText().trim())) {
            throw new ContenidoInvalidoExcepcion("El usuario ingresado ya existe");
        }

        boolean res = validarCedula();
        return res;

    }

    /**
     * Método que se encarga de cofirmar si la clave proporcionada cumple con
     * las restricciones para guardarse
     *
     * @return Retorna true, si la clave es válida, en caso contrario, retorna
     * false
     * @throws CamposVaciosExcepcion Excepcion lanzada cuando los campos a
     * llenar están vacios
     * @throws ContenidoInvalidoExcepcion Excepcion lanzada cuando el contenido
     * de los campos es invalido
     */
    private Boolean confirmarClave() throws ClaveIncorrectoExcepcion, ContenidoInvalidoExcepcion {
        if (!String.valueOf(txtClave.getPassword()).trim().equals(String.valueOf(txtConfirmarClave.getPassword()).trim())) {
            throw new ClaveIncorrectoExcepcion("Fallo al confirmar la contraseña");
        } else if (String.valueOf(txtClave.getPassword()).trim().length() < 6 || String.valueOf(txtClave.getPassword()).trim().length() > 16) {
            throw new ContenidoInvalidoExcepcion("La clave solo puede ser de 6 a 16 caracteres");
        } else {
            return true;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtNombreEmpleado = new javax.swing.JTextField();
        txtCedulaEmpleado = new javax.swing.JTextField();
        txtApellidoEmpleado = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbRolEmpleado = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        btnCancelar = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        txtClave = new javax.swing.JPasswordField();
        txtConfirmarClave = new javax.swing.JPasswordField();
        txtUsuario = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setLayout(null);

        txtNombreEmpleado.setText("jTextField1");
        jPanel1.add(txtNombreEmpleado);
        txtNombreEmpleado.setBounds(20, 100, 350, 25);

        txtCedulaEmpleado.setText("jTextField1");
        jPanel1.add(txtCedulaEmpleado);
        txtCedulaEmpleado.setBounds(20, 170, 350, 25);

        txtApellidoEmpleado.setText("jTextField1");
        jPanel1.add(txtApellidoEmpleado);
        txtApellidoEmpleado.setBounds(390, 100, 350, 25);

        jLabel2.setText("Apellidos:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(390, 80, 68, 19);

        jLabel4.setText("Rol:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(390, 150, 26, 19);

        cmbRolEmpleado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(cmbRolEmpleado);
        cmbRolEmpleado.setBounds(390, 170, 350, 25);

        jLabel3.setText("Cédula:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(20, 150, 49, 19);

        jLabel1.setText("Nombres:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 80, 67, 19);

        jLabel9.setFont(new java.awt.Font("Cantarell", 1, 24)); // NOI18N
        jLabel9.setText("Empleado");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(320, 20, 109, 30);
        jPanel1.add(jSeparator3);
        jSeparator3.setBounds(10, 60, 760, 10);

        btnCancelar.setText("Cancelar");

        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        jPanel2.setLayout(null);

        jLabel8.setFont(new java.awt.Font("Cantarell", 1, 24)); // NOI18N
        jLabel8.setText("Cuenta");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(340, 20, 109, 30);
        jPanel2.add(jSeparator2);
        jSeparator2.setBounds(0, 60, 760, 10);

        txtClave.setText("jPasswordField1");
        jPanel2.add(txtClave);
        txtClave.setBounds(200, 130, 270, 25);

        txtConfirmarClave.setText("jPasswordField1");
        jPanel2.add(txtConfirmarClave);
        txtConfirmarClave.setBounds(200, 180, 270, 25);

        txtUsuario.setText("jTextField5");
        jPanel2.add(txtUsuario);
        txtUsuario.setBounds(200, 90, 270, 25);

        jLabel5.setText("Usuario:");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(20, 100, 54, 19);

        jLabel6.setText("Contraseña:");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(20, 140, 79, 19);

        jLabel7.setText("Confirmar contraseña:");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(20, 180, 145, 19);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(565, Short.MAX_VALUE)
                .addComponent(btnAceptar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addGap(39, 39, 39))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar)
                    .addComponent(btnAceptar))
                .addGap(15, 15, 15))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Método que se encarga de guardar los datos ingresados en el formulario
     *
     * @param evt
     */
    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        try {
            if (verificarCampos() && confirmarClave()) {
                controladorCuenta.insertarCuenta(txtUsuario.getText().trim(),
                        String.valueOf(txtClave.getPassword()).trim(),
                        controladorPersona.insertarPersona(txtNombreEmpleado.getText().trim(),
                                txtApellidoEmpleado.getText().trim(),
                                txtCedulaEmpleado.getText().trim(),
                                controladorRol.buscarRolPorNombre((String) cmbRolEmpleado.getSelectedItem())));

                controladorCuenta.getCuentaList().print();
                controladorPersona.getPersonaList().print();
                //TODO falta actualizar archivo de cuenta y persona
                Utilidades.guardarJson(controladorPersona, "personas");
                Utilidades.guardarJson(controladorCuenta, "cuentas");
                this.dispose();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaRegistroCuentx.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaRegistroCuentx.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaRegistroCuentx.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaRegistroCuentx.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VistaRegistroCuentx dialog = new VistaRegistroCuentx(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox<String> cmbRolEmpleado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTextField txtApellidoEmpleado;
    private javax.swing.JTextField txtCedulaEmpleado;
    private javax.swing.JPasswordField txtClave;
    private javax.swing.JPasswordField txtConfirmarClave;
    private javax.swing.JTextField txtNombreEmpleado;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
