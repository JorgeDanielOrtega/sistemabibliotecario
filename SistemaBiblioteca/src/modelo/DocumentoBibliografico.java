/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDate;

/**
 *
 * @author daniel
 */
public class DocumentoBibliografico {

    private Integer id;
    private String idioma;
    private String titulo;
    private String editorial;
    private String autor;
    private Integer paginas;
    private LocalDate anioPublicacion;

    public DocumentoBibliografico() {
    }

    public DocumentoBibliografico(Integer id, String idioma, String titulo, String editorial, String autor, Integer paginas, LocalDate anioPublicacion) {
        this.id = id;
        this.idioma = idioma;
        this.titulo = titulo;
        this.editorial = editorial;
        this.autor = autor;
        this.paginas = paginas;
        this.anioPublicacion = anioPublicacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Integer getPaginas() {
        return paginas;
    }

    public void setPaginas(Integer paginas) {
        this.paginas = paginas;
    }

    public LocalDate getAnioPublicacion() {
        return anioPublicacion;
    }

    public void setAnioPublicacion(LocalDate anioPublicacion) {
        this.anioPublicacion = anioPublicacion;
    }

    @Override
    public String toString() {
        return "DocumentoBibliografico{" + "id=" + id + ", idioma=" + idioma + ", titulo=" + titulo + ", editorial=" + editorial + ", autor=" + autor + ", paginas=" + paginas + ", anioPublicacion=" + anioPublicacion + '}';
    }

}
