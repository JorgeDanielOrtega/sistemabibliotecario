/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author daniel
 */
public class Rol {

    private Integer id;
    private String nombre;
    private String descripcion;

    public Rol() {
    }

    public Rol(Integer id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
    /**
     * Presenta un formato que se usa para el mostrar el registro de los roles en formato de PDF
     * @return 
     */
     public String presentar() {
        StringBuilder formato = new StringBuilder();
        formato.append("\tID: ").append(id).append("\n");
        formato.append("\tNombre: ").append(nombre).append("\n");
        formato.append("\tDescripcion: ").append(descripcion).append("\n");
        return formato.toString();
    }
     
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Rol{" + "id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + '}';
    }
  
}
