/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDate;

/**
 *
 * @author daniel
 */
public class Prestamo {

    private Integer id;
    private Integer dias;
    private Persona persona;
    private DocumentoBibliografico documentoBibliografico;
    private LocalDate fechaPrestamo;
    private LocalDate fechaDevolucion;

    public Prestamo() {
    }

    public Prestamo(Integer id, Integer dias, Persona persona, DocumentoBibliografico documentoBibliografico) {
        this.id = id;
        this.dias = dias;
        this.persona = persona;
        this.documentoBibliografico = documentoBibliografico;
        this.fechaPrestamo = asignarFechaActual();
        this.fechaDevolucion = calcularFechaDevolucion();
    }
    
     public Prestamo(Integer dias, Persona persona, DocumentoBibliografico documentoBibliografico) {
        this.id = id;
        this.dias = dias;
        this.persona = persona;
        this.documentoBibliografico = documentoBibliografico;
        this.fechaPrestamo = asignarFechaActual();
        this.fechaDevolucion = calcularFechaDevolucion();
    }

    /**
     * Presenta en un formato aceptable al prestamo
     *
     * @return Retorna el formato del prestamo
     */
    public String presentar() {
        StringBuilder formato = new StringBuilder();
        formato.append("\t" + "Dias prestamo: " + dias + "\n");
        formato.append("\t" + "Cliente: " + persona.getNombres()+" "+persona.getApellidos() + "\n");
        formato.append("\t" + "Documento bibliografico: " + documentoBibliografico.getTitulo() + "\n");
        return formato.toString();
    }
    
     private LocalDate calcularFechaDevolucion() {
        return fechaPrestamo.plusDays(Long.parseLong(dias.toString()));
    }

    private LocalDate asignarFechaActual() {
        return LocalDate.now();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDias() {
        return dias;
    }

    public void setDias(Integer dias) {
        this.dias = dias;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public DocumentoBibliografico getDocumentoBibliografico() {
        return documentoBibliografico;
    }

    public void setDocumentoBibliografico(DocumentoBibliografico documentoBibliografico) {
        this.documentoBibliografico = documentoBibliografico;
    }

    public LocalDate getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(LocalDate fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public LocalDate getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(LocalDate fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    @Override
    public String toString() {
        return "Prestamo{" + "id=" + id + ", dias=" + dias + ", persona=" + persona + ", documentoBibliografico=" + documentoBibliografico + ", fechaPrestamo=" + fechaPrestamo + ", fechaDevolucion=" + fechaDevolucion + '}';
    }

}
