/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author daniel
 */
public class Sancion {

    private Integer id;
    private Integer diasSancion;
    private Integer diasRestantes;
    private Prestamo prestamo;

    public Sancion() {
    }

    public Sancion(Integer id, Integer diasSancion, Integer diasRestantes, Prestamo prestamo) {
        this.id = id;
        this.diasSancion = diasSancion;
        this.diasRestantes = diasRestantes;
        this.prestamo = prestamo;
    }

     /**
     * Presenta en un formato aceptable a la sancion
     *
     * @return Retorna el formato de la sancion
     */
    public String presentar() {
        StringBuilder formato = new StringBuilder();
        formato.append("\tDias prestamo: ").append(diasRestantes).append("\n");
        formato.append("\tCliente: ").append(diasSancion).append("\n");
        formato.append("\tDocumento bibliografico: ").append(prestamo.getDocumentoBibliografico()).append("\n");
        return formato.toString();
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDiasSancion() {
        return diasSancion;
    }

    public void setDiasSancion(Integer diasSancion) {
        this.diasSancion = diasSancion;
    }

    public Integer getDiasRestantes() {
        return diasRestantes;
    }

    public void setDiasRestantes(Integer diasRestantes) {
        this.diasRestantes = diasRestantes;
    }

    public Prestamo getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(Prestamo prestamo) {
        this.prestamo = prestamo;
    }

    @Override
    public String toString() {
        return "Sancion{" + "id=" + id + ", diasSancion=" + diasSancion + ", diasRestantes=" + diasRestantes + ", prestamo=" + prestamo + '}';
    }

}
