/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDate;

/**
 *
 * @author daniel
 */
public class Libro extends DocumentoBibliografico {

    private Integer edicion;
    private String lugarPublicacion;

    public Libro() {
    }

    public Libro(Integer id, String idioma, String titulo, String editorial, String autor, Integer paginas, LocalDate anioPublicacion) {
        super(id, idioma, titulo, editorial, autor, paginas, anioPublicacion);
    }

    
    public Libro(Integer edicion, String lugarPublicacion, Integer id, String idioma, String titulo, String editorial, String autor, Integer paginas, LocalDate anioPublicacion) {
        super(id, idioma, titulo, editorial, autor, paginas, anioPublicacion);
        this.edicion = edicion;
        this.lugarPublicacion = lugarPublicacion;
    }
    
    public String presentar() {
        StringBuilder formato = new StringBuilder();
        formato.append("\t" + "ID: " + getId().toString() + "\n");
        formato.append("\t" + "Titulo: " + getTitulo() + "\n");
        formato.append("\t" + "Idioma: " + getIdioma() + "\n");
        formato.append("\t" + "Editorial: " + getEditorial() + "\n");
        formato.append("\t" + "Autor: " + getAutor() + "\n");
        formato.append("\t" + "Paginas: " + getPaginas() + "\n");
        formato.append("\t" + "Año Publicacion: " + getAnioPublicacion().getYear() + "\n");
        formato.append("\t" + "Edicion: " + edicion + "\n");
        formato.append("\t" + "Lugar de Publicacion: " + lugarPublicacion + "\n");

        return formato.toString();
    }

    public Integer getEdicion() {
        return edicion;
    }

    public void setEdicion(Integer edicion) {
        this.edicion = edicion;
    }

    public String getLugarPublicacion() {
        return lugarPublicacion;
    }

    public void setLugarPublicacion(String lugarPublicacion) {
        this.lugarPublicacion = lugarPublicacion;
    }

    @Override
    public String toString() {
        return "Libro{" + "edicion=" + edicion + ", lugarPublicacion=" + lugarPublicacion + '}';
    }

}
