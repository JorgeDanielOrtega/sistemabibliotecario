/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Base64;

/**
 * Clase Cuenta
 *
 * @author daniel
 */
public class Cuenta {

    /**
     * Id de la cuenta
     */
    private Integer id;
    /**
     * Usuario de la cuenta
     */
    private String usuario;
    /**
     * Clave de la cuenta
     */
    private String clave;
    /**
     * Estado de la cuenta
     */
    private Boolean estaActiva;
    /**
     * Datos de la persona de la cuenta
     */
    private Persona persona;

    /**
     * Constructor vacio por defecto
     */
    public Cuenta() {
    }

    /**
     * Constructor que crea un nueo objeto de la clase Cuenta
     *
     * @param id Id de la cuenta
     * @param usuario Usuario de la cuenta
     * @param clave Clave de la cuenta
     * @param estaActiva Estado de la cuenta
     * @param persona Datos de la persona de la cuenta
     */
    public Cuenta(Integer id, String usuario, String clave, Boolean estaActiva, Persona persona) {
        this.id = id;
        this.usuario = usuario;
        this.clave = encriptarClave(clave);
        this.estaActiva = estaActiva;
        this.persona = persona;
    }

    /**
     * Encripta la clave enviada como parametro
     *
     * @param clave Clave a encriptar
     * @return Retorna la clave encriptada si no esta vacia, por el contrario,
     * retorna un String vacio
     */
    private String encriptarClave(String clave) {
        if (clave != null) {
            return Base64.getEncoder().encodeToString(clave.getBytes());
        }
        return "";
    }

    /**
     * Desencripta la clave
     *
     * @return Retorna la clave desencriptada
     */
    public String desencriptarClave() {
        return new String(Base64.getDecoder().decode(this.clave));
    }

    /**
     * Presenta en un formato aceptable a la cuenta
     *
     * @return Retorna el formato de la cuenta
     */
    public String presentar() {
        StringBuilder formato = new StringBuilder();
        formato.append("\t" + "Usuario: " + usuario + "\n");
        formato.append("\t" + "Activa: " + estaActiva + "\n");
        formato.append("\t" + "Persona: " + persona.getCedula() + "\n");

        return formato.toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = encriptarClave(clave);
    }

    public Boolean getEstaActiva() {
        return estaActiva;
    }

    public void setEstaActiva(Boolean estaActiva) {
        this.estaActiva = estaActiva;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public String toString() {
        return "Cuenta{" + "id=" + id + ", usuario=" + usuario + ", clave=" + clave + ", estaActiva=" + estaActiva + ", persona=" + persona + '}';
    }

}
