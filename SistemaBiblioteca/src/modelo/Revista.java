/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDate;

/**
 *
 * @author daniel
 */
public class Revista extends DocumentoBibliografico {

    private Integer volumen;
    private String isbn;
    private LocalDate mesPublicacion;

    public Revista() {
    }

    public Revista(Integer volumen, String isbn, LocalDate mesPublicacion, Integer id, String idioma, String titulo, String editorial, String autor, Integer paginas, LocalDate anioPublicacion) {
        super(id, idioma, titulo, editorial, autor, paginas, anioPublicacion);
        this.volumen = volumen;
        this.isbn = isbn;
        this.mesPublicacion = mesPublicacion;
    }

    public String presentar() {
        StringBuilder formato = new StringBuilder();
        formato.append("\t" + "ID: " + getId().toString() + "\n");
        formato.append("\t" + "ISBN: " + isbn + "\n");
        formato.append("\t" + "Titulo: " + getTitulo() + "\n");
        formato.append("\t" + "Volumen: " + volumen + "\n");
        formato.append("\t" + "Idioma: " + getIdioma() + "\n");
        formato.append("\t" + "Editorial: " + getEditorial() + "\n");
        formato.append("\t" + "Autor: " + getAutor() + "\n");
        formato.append("\t" + "Paginas: " + getPaginas() + "\n");
        formato.append("\t" + "Año Publicacion: " + getAnioPublicacion().getYear() + "\n");
        formato.append("\t" + "Mes de publicacion: " + mesPublicacion.getMonth().toString() + "\n");

        return formato.toString();
    }

    public Integer getVolumen() {
        return volumen;
    }

    public void setVolumen(Integer volumen) {
        this.volumen = volumen;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public LocalDate getMesPublicacion() {
        return mesPublicacion;
    }

    public void setMesPublicacion(LocalDate mesPublicacion) {
        this.mesPublicacion = mesPublicacion;
    }

    @Override
    public String toString() {
        return "Revista{" + "volumen=" + volumen + ", isbn=" + isbn + ", mesPublicacion=" + mesPublicacion + '}';
    }

}
