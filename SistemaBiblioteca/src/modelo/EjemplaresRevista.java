/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author Usuario
 */
public class EjemplaresRevista {
    private Integer id;
    private Integer nro_ejemplar;

    public EjemplaresRevista() {
    }

    
    public EjemplaresRevista(Integer id, Integer nro_ejemplar) {
        this.id = id;
        this.nro_ejemplar = nro_ejemplar;
    }
    
    public String presentar() {
        StringBuilder formato = new StringBuilder();
        formato.append("\t" + "ID: " + id.toString() + "\n");
        formato.append("\t" + "Numero de ejemplares: " + nro_ejemplar.toString() + "\n");
        return formato.toString();
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNro_ejemplar() {
        return nro_ejemplar;
    }

    public void setNro_ejemplar(Integer nro_ejemplar) {
        this.nro_ejemplar = nro_ejemplar;
    }

    
    @Override
    public String toString() {
        return "EjemplaresRevista{" + "id=" + id + ", nro_ejemplar=" + nro_ejemplar + '}';
    }
    
    
}
