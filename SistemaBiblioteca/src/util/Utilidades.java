/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import controlador.ControladorRol;
import java.lang.reflect.Field;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * Clase que contiene metodos utiles para el desarrollo del proyecto
 *
 * @author daniel
 */
public class Utilidades {

    private static final Integer A_MINUSCULA = 97;
    private static final Integer Z_MINUSCULA = 122;
    private static final Integer A_MAYUSCULA = 65;
    private static final Integer Z_MAYUSCULA = 90;
    private static final Integer N_MAYUSCULA = 165;
    private static final Integer N_MINUSCULA = 164;
    private static final Integer ESPACIO = 32;
    private static final Integer NUM_CERO = 48;
    private static final Integer NUM_NUEVE = 57;

    /**
     * Carga el json indicandole la clase y el nombre del archivo con el cual
     * fue guardado
     *
     * @param clazz Clase necesaria para la carga del archivo
     * @param nombreArchivo Nombre del archivo con el que se guardo el json en
     * la carpeta data
     * @return Si salio algo mal, se retorna null, por el contrario, se
     * retornará los datos del json en forma de objeto
     */
    public static Object cargarJson(Class clazz, String nombreArchivo) {
        return new FileJSON(nombreArchivo).cargar(clazz);
    }

    /**
     * Guarda en un json un objeto
     *
     * @param controlador Es el objeto a guardar
     * @param nombreArchivo Es el nombre del archivo con el que se va guardar el
     * json
     * @return Retorna un booleano indicando el estado de la accion, si es true,
     * todo ha ido bien, si es false, algo ha salido mal
     */
    public static Object guardarJson(Object controlador, String nombreArchivo) {
        return new FileJSON(nombreArchivo).guardar(controlador);
    }

    /**
     * Recibe un JComboBox, y lo rellena con los atributos del libro
     *
     * @param cmb JComboBox pasado como parametro
     */
    public static void cargarComboAtributoLibro(JComboBox cmb) {
        cmb.removeAllItems();
        cmb.addItem("ID");
        cmb.addItem("Idioma");
        cmb.addItem("Titulo");
        cmb.addItem("Editorial");
        cmb.addItem("Autor");
        cmb.addItem("lugarPublicacion");

    }

    /**
     * Recibe un JComboBox, y lo rellena con los atributos de la revista
     *
     * @param cmb JComboBox pasado como parametro
     */
    public static void cargarComboAtributoRevista(JComboBox cmb) {
        cmb.removeAllItems();
        cmb.addItem("ID");
        cmb.addItem("Titulo");
        cmb.addItem("Autor");
        cmb.addItem("Editorial");
        cmb.addItem("ISBN");

    }
    
    /**
     * Recibe un JSpinner y limita el valor minimo que puede tomar
     * 
     * @param spn 
     */
    public static void limitarSpinner(JSpinner spn){
        SpinnerNumberModel modeloSpinner = new SpinnerNumberModel();
        modeloSpinner.setMinimum(0);
        spn.setModel(modeloSpinner);
    }

    /**
     * Carga el JComboBox pasado como parametro y lo rellena con los atributos
     * de persona
     *
     * @param cmb JComboBox pasado como parametro
     */
    public static void cargarComboAtributosPersona(JComboBox cmb) {
        cmb.removeAllItems();
        cmb.addItem("Id");
        cmb.addItem("Nombres");
        cmb.addItem("Apellidos");
        cmb.addItem("Cedula");
        cmb.addItem("Rol");
    }
    /**
     * Recibe un JComboBox, y lo rellena con los atributos para la cuenta
     *
     * @param cmb JComboBox pasado como parametro
     */
    public static void cargarComboAtributosCuenta(JComboBox cmb){
        cmb.removeAllItems();
        cmb.addItem("ID");
        cmb.addItem("Usuario");
    }

    /**
     * Carga el JComboBox pasado como parametro y lo rellena con los roles
     * existentes en el archivo roles.json
     *
     * @param combo JComboBox pasado como parametro
     */
    public static void cargarCombo(JComboBox combo) {
        ControladorRol controladorRol = new FileJSON("roles").cargar(ControladorRol.class);
        combo.removeAllItems();
        try {
            for (int i = 0; i < controladorRol.getRolList().getSize(); i++) {
                combo.addItem(controladorRol.getRolList().obtener(i).getNombre());
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Capitaliza un cadena
     *
     * @param cadena Cadena pasada como parametro
     * @return Retorna la cadena capitalizada
     */
    public static String capitalizar(String cadena) {
        if (!cadena.trim().equalsIgnoreCase("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            cadenaChar[0] = Character.toUpperCase(cadenaChar[0]);
            for (int i = 0; i < cadenaChar.length; i++) {
                if (cadenaChar[i] == ' ') {
                    cadenaChar[i + 1] = Character.toUpperCase(cadenaChar[i + 1]);
                }
            }
            return String.valueOf(cadenaChar);
        }
        return cadena;
    }

    /**
     * Capitaliza solo la primera letra de una cadena
     *
     * @param cadena Cadena pasada como parametro
     * @return Retorna la cadena con la primera letra capitalizada
     */
    public static String capitalizarPrimeraPalabra(String cadena) {
        if (!cadena.trim().equalsIgnoreCase("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            cadenaChar[0] = Character.toUpperCase(cadenaChar[0]);
            return String.valueOf(cadenaChar);
        }
        return cadena;
    }

    /**
     * Verifica si en la cadena, existen espacios juntos
     *
     * @param cadena Cadena pasada como parametro
     * @return Retornar un true, en caso exista en la cadena espacios juntos, o
     * un false, en caso no haya encontrado ninguna coincidencia
     */
    public static Boolean hayEspaciosJuntos(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {

                if (cadenaChar[i] == ' ') {
                    if (cadenaChar[i + 1] == ' ') {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    /**
     * Verifica si una cadena contiene espacios
     *
     * @param cadena Cadena pasada como parametro
     * @return Retornar un true, en caso existan espacios , o un false, en caso
     * no haya encontrado ninguna coincidencia
     */
    public static Boolean hayEspacios(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                if (cadenaChar[i] == ' ') {
                    return true;
                }
            }
//            return false;
        }
        return false;
    }

    /**
     * Verifica si una cadena contiene caracteres no validos, siendo estos
     * cualquier caracter que no sea una letra(mayúscula o minúscula) o un
     * espacio
     *
     * @param cadena Cadena pasada como parametro
     * @return Retornar un true, en caso existan caracteres no validos , o un
     * false, en caso no haya encontrado ninguna coincidencia
     */
    public static Boolean hayCaracteresNoValidos(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (!((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii == N_MAYUSCULA) || (posicionAscii == N_MINUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA)
                        || posicionAscii == ESPACIO)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Verifica si la cadena contiene solo letras(mayúscula o minúscula)
     *
     * @param cadena Cadena pasada como parametro
     * @return Retornar un true, en caso la cadena solo contenga letras, o un
     * false, si se da el caso contrario
     */
    public static Boolean haySoloLetras(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA))) {
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Verifica si la cadena contiene solo letras(mayúscula o minúscula),
     * descartando el caracter especificado como parametro
     *
     * @param cadena Cadena pasada como parametro
     * @param characterPersonalizado Caracter que sera descartado en el proceso
     * @return Retornar un true, en caso la cadena solo contenga letras,
     * omitiendo el caracter especificado, o un false, en el caso contrario
     */
    public static Boolean haySoloLetrasConCharacterPersonalizado(String cadena, char characterPersonalizado) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii == N_MAYUSCULA) || (posicionAscii == N_MINUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA)) || posicionAscii == (int) characterPersonalizado) {
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Verifica si una cadena contiene caracteres no validos(caracteres
     * diferentes a una letra o numero), a excepcion de los numeros
     *
     * @param cadena Cadena pasada como parametro
     * @return Retornar un true, en caso la cadena contenga un caracter no null
     * valido, exceptuando los numeros, o un false, en el caso de que no se haya
     * encontrado ninguna coincidencia
     */
    public static Boolean hayCaracteresNoValidosExcepcionNumeros(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if ((posicionAscii >= NUM_CERO && posicionAscii <= NUM_NUEVE)) {
                    continue;
                }
                if (!((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA)
                        || (posicionAscii == N_MAYUSCULA) || (posicionAscii == N_MINUSCULA)
                        || posicionAscii == ESPACIO)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Verifica que la cadena solo contenga números
     *
     * @param cadena Cadena pasada como parametro
     * @return Retornar un true, en caso la cadena solo contenga números, o un
     * false, en el caso contrario
     */
    public static Boolean haySoloNumeros(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (!(posicionAscii >= NUM_CERO && posicionAscii <= NUM_NUEVE)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Verifica si la cadena contiene las n palabras especificadas
     *
     * @param cadena Cadena pasada como parametro
     * @param maxpalabras Máximo de palabras que debe de contener la cadena
     * @return Retornar un true, en caso la cadena contenga las n palabras
     * especificadas, o un false, en el caso contrario
     */
    public static Boolean hayNPalabras(String cadena, int maxpalabras) {
        if (!cadena.equals("") && maxpalabras > 0) {
            char[] cadenaChar = cadena.trim().toCharArray();
            int numEspacios = 0;
            for (int i = 0; i < cadenaChar.length; i++) {
                if (cadenaChar[i] == ' ') {
                    numEspacios++;
                }
            }
            return (numEspacios + 1) == maxpalabras;
        }
        return false;
    }

    /**
     * Valida si la cadena esta llena de numeros iguales
     *
     * @param cadena Cadena pasada como parametro
     * @return Retornar un true, en caso la cadena contenga los mismo numeros, o
     * un false, en el caso contrario
     */
    public static Boolean numerosIguales(String cadena) {
        if (!cadena.equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            char comparaCharacter = cadenaChar[0];
            boolean res = true;
            for (int i = 0; i < cadenaChar.length; i++) {
                if (cadenaChar[i] != comparaCharacter) {
                    res = false;
                    break;
                }
            }
            return res;
        }

        return true;
    }

    /**
     *  Verifica si la clase enviada como parametro es de tipo
     * Number
     *
     * @param clase Clase a evaluar
     * @return Retorna un true, en caso la clase sea Number, o un false, en el
     * caso contrario
     */
    public static Boolean isNumber(Class clase) {
        return clase.getSuperclass().getSimpleName().equalsIgnoreCase("Number");
    }

    /**
     * Verifica si la clase enviada como parametro es de tipo String
     *
     * @param clase Clase a evaluar
     * @return Retorna un true, en caso la clase sea String, o un false, en el
     * caso contrario
     */
    public static Boolean isString(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("String");
    }

    /**
     * Verifica si la clase enviada como parametro es de tipo Character
     *
     * @param clase Clase a evaluar
     * @return Retorna un true, en caso la clase sea Character, o un false, en
     * el caso contrario
     */
    public static Boolean isCharacter(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("Character");
    }

    /**
     * Verifica si la clase enviada como parametro es de tipo Boolean
     *
     * @param clase Clase a evaluar
     * @return Retorna un true, en caso la clase sea Boolean, o un false, en el
     * caso contrario
     */
    public static Boolean isBoolean(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("Boolean");
    }

    /**
     * Verifica si la clase enviada como parametro es de tipo Primitive
     *
     * @param clase Clase a evaluar
     * @return Retorna un true, en caso la clase sea Primitive, o un false, en
     * el caso contrario
     */
    public static Boolean isPrimitive(Class clase) {
        return clase.isPrimitive();
    }

    /**
     * Verifica si la clase enviada como parametro es de tipo Object
     *
     * @param clase Clase a evaluar
     * @return Retorna un true, en caso la clase sea Object, o un false, en el
     * caso contrario
     */
    public static Boolean isObject(Class clase) {
        return (!isBoolean(clase) && !isCharacter(clase)
                && !isNumber(clase) && !isString(clase) && !isPrimitive(clase)
                && !isEnum(clase));
    }

    /**
     * Verifica si la clase enviada como parametro es de tipo Enum
     *
     * @param clase Clase a evaluar
     * @return Retorna un true, en caso la clase sea Enum, o un false, en el
     * caso contrario
     */
    public static Boolean isEnum(Class clase) {
        return clase.getSuperclass().getSimpleName().toLowerCase().contains("enum");
    }

    /**
     * Obtiene el atributo de la clase pasada como parametro
     *
     * @param clase Clase que contiene el atributo a extraer
     * @param nombre Nombre del atributo a extraer
     * @return Retorna el atributo en caso se haya encontrado, por el contrario,
     * retorna null
     */
    public static Field obtenerAtributo(Class clase, String nombre) {
        Field atributo = null;
        for (Field aux : clase.getDeclaredFields()) {
            if (aux.getName().equalsIgnoreCase(nombre)) {
                atributo = aux;
                break;
            }
        }
        return atributo;
    }

}
